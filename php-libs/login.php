<?php
require_once("../php-libs/function.ConnDB.php");
$Username = $_POST['usuario'];
$Password = $_POST['password'];

$s = "SELECT user_Userid, user_Login, user_Email, user_Fullname, user_SubType, user_Avatar, DATE_FORMAT(user_Creation,'%Y-%m-%d') AS Fecha, user_Status FROM Users WHERE (user_Login='".$Username."' OR user_Email='".$Username."') AND user_Password='".$Password."'";
$q = $DB->query($s);
$t = $q->num_rows;
if($t>0){
	$r = $q->fetch_array();
	$DateCreation = $r['Fecha'];
	$DateLimit = date('Y-m-d', strtotime($r['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
	session_start();
	$_SESSION['SessionUserID'] = $r['user_Userid'];
	$_SESSION['SessionLogin'] = $r['user_Login'];
	$_SESSION['SessionFullname'] = $r['user_Fullname'];
	$_SESSION['SessionAvatar'] = $r['user_Avatar'];
	// die(var_dump($DateLimit, $Today, $r['user_Status']));
	// if($r['user_Password']==$Password){
		// if(($r['user_Status']==0 && $DateLimit>=$Today) || ($r['user_Status']==1)){
		if(validUserLogin($r['user_Userid'], $r['user_SubType'], $r['Fecha'], $r['user_Status'])) {
			$_SESSION['SessionSuscribed'] = true;
			header('Location: ' . BASE_URL . '/convoy-webapp/welcome');
		} elseif ($r['user_SubType']=='i') {
            header('Location: ' . BASE_URL . '/convoy-webapp/io-purchase');
        } else{
			$_SESSION['SessionSuscribed'] = false;
			header('Location: ' . BASE_URL . '/convoy-webapp/open-pay');
		}
    }
    
else{
	$AppResponse = 'Usuario o contraseña incorrectos';
	header('Location: ' . BASE_URL . '/convoy-webapp/login?error='.$AppResponse);
	}

function validUserLogin($userId, $userSubType, $creationDate, $userStatus) {
	$FechaLimite = date('Y-m-d', strtotime($creationDate. ' + 7 days'));
	$Today = date('Y-m-d');
	if ($userStatus == 0 || $FechaLimite <= $Today) {
		switch ($userSubType) {
			case 'a':
			case 'w':
				// ask for openpay status
				$subscription = getCustomerSubscriptionId($userId);
				return ($subscription['AppStatus'] == '0') ? false : true;
				break;
			case 'i':
				// ask for ioPurchase
				$ioPurchase = PurchaseReviewGeneral($userId);
				return ($ioPurchase['SubscriptionStatus'] == '0') ? fasle : true;
				break;
			default:
				// ask for what is already there
				// could it happen that a non 'wai' user can be an active
				// user??
				return ($userStatus == 1 || $FechaLimite > $Today);
				break;
		}
	} else {
		return true;
	}
		
}

function PurchaseReviewGeneral($UserID) {
	$DB = $GLOBALS['DB'];

    $Message=array();
    
    // $DB = ConnectDB();
    $Today = date('Y-m-d H:i');
    $s1 = "SELECT ".
    "Users.user_UserID as UserID, ".
    "Users.user_SubType AS SubType, ".
    "iTunes_Subscription.iTSub_Expire ".
    "FROM iTunes_Subscription ".
    "INNER JOIN Users ON iTunes_Subscription.iTSub_UserID = Users.user_UserID ".
    "WHERE iTSub_UserID = ". $UserID." ORDER BY iTunes_Subscription.id_iTSub DESC LIMIT 1";
    $q1 = $DB->query($s1);
    $r1 = $q1->fetch_array();
    $t1 = $q1->num_rows;
    
    if($t1>0){
        
        $Message['SubType'] = $r1['SubType'];
        if($Message['SubType'] == "")
            $Message['SubType'] = "n";
        
        $datetime2 = new DateTime($r1['iTSub_Expire']);
        $datetime1 = new DateTime($Today);
        $interval = $datetime1->diff($datetime2);
        $Transcurridos = $interval->format('%R%a');
        
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La suscripción es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            http_response_code(402);//'Payment Required'
            $Message['SubscriptionMessage'] = "La suscripción ha expirado.";
        }

    }
    else{
        
        $s2 = "SELECT ".
        "user_SubType AS SubType, ".
        "user_Creation AS CreationDate ".
        "FROM Users ".
        "WHERE user_UserID = ". $UserID;
        $q2 = $DB->query($s2);
        $r2 = $q2->fetch_array();
        $t2 = $q2->num_rows;
        
        //Expire Date + date formtatting + add 7 days for trial
        $date = date_create($r2['CreationDate']);
        $date = date('Y-m-d', strtotime($r2['CreationDate']));
        $date = date("Y-m-d", strtotime("+7 day", strtotime($r2['CreationDate']) ));

        //Días transcurridos de prueba
        $datetime2 = new DateTime($date);
        $datetime1 = new DateTime($Today);
        
        $interval = $datetime1->diff($datetime2);
        $Transcurridos = $interval->format('%R%a');
 
        $Message['SubType'] = $r2['SubType'];
        if($r2['SubType'] == "")
            $Message['SubType'] = "n";
        
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La la semana de prueba es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            http_response_code(402);//'Payment Required'
            $Message['SubscriptionMessage'] = "La semana de prueba ha expirado.";
        }
        //$Message['AppResponse']="No existe registro del pago.";
    }//else from if t0
     
    //print_r(json_encode($Message, JSON_PRETTY_PRINT));
    return $Message;
    $DB->close();
   
}

function getCustomerSubscriptionId($userId)
{
	$DB = $GLOBALS['DB'];
	$sql = "SELECT * FROM openpay_subscriptions WHERE UserID = " . $userId ."
		ORDER BY Creation_date DESC, id DESC LIMIT 1;";
		$qry = $DB->query($sql);
		$total = $qry->num_rows;
		$userSubsription = $qry->fetch_array(MYSQLI_ASSOC);
		if ($total == 0){
			return array('AppStatus'=>'0', 'AppResponse' => 'El usuario no esta suscrito a ningun contenido');
		} elseif ($userSubsription['Status'] == 'canceled') {
			return array('AppStatus'=>'0', 'AppResponse' => 'El usuario ha cancelado la suscripción');
		} else {
			return array(
				'AppStatus' => '1',
				'AppResponse' => 'Este Usuario se ha suscrito a algo.',
				'SubscriptionID' => $userSubsription['SubscriptionID'],
			);
		}
	$DB->close();
	// return $this->openpay->customers->add($customerData);
}
