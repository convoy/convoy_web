<?php
 
function error($variable) {
	$mensaje_e=array();
	$mensaje_e['app_status']=0;
	$mensaje_e['mensaje']=$variable;
	return $mensaje_e;
}
function crear_pag($id_usuario, $nombre, $cc, $mes_expira, $anio_expira, $secure_cc, $cp, $periodo) {
		$base_datos=conexion();
		$mensaje=array();

		$seleccionar_otros_pagos="SELECT * FROM pagos WHERE id_usuario='".$id_usuario."'";
		$seleccionando_otros_pagos=$base_datos->query($seleccionar_otros_pagos);
		$num_pagos=$seleccionando_otros_pagos->num_rows;

		$seleccionar_usuario="SELECT * FROM usuarios WHERE id_usuario='".$id_usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);
		$fila_usuario=$seleccionando_usuario->fetch_array();


		if ($num_pagos==0) {
		$periodo_prueba=date($periodo, strtotime('+7 days'));
		$tipo_pago=0;
		$periodo_prox=date($periodo, strtotime("+14 days")); } else { 
		$periodo_prox=date($periodo, strtotime('+30 days'));
		}

try {

	 $openpay = Openpay::getInstance('m1zbkbapzduqz7jtwp1p', 
	                                 'sk_c2dbe2d1e4574a568da2e09d1308737b'); 

	 $customerData = array(
     'name' => $nombre,
     'email' => $fila_usuario['email']
      );

	 $customer = $openpay->customers->add($customerData);
 $anio_exp=str_split($anio_expira, 2);
 // echo $mes_expira."-".$anio_exp[1];
$cardDataRequest = array(
    'holder_name' => $nombre,
    'card_number' => $cc,
    'cvv2' => $secure_cc,
    'expiration_month' => $mes_expira,
    'expiration_year' => $anio_exp[1]);

$customer = $openpay->customers->get($customer->id);
$card = $customer->cards->add($cardDataRequest);

$subscriptionDataRequest = array(
    "trial_end_date" => $periodo_prox, 
    'plan_id' => 'p9lkmhroltzbujjt5qpu',
    'card_id' => $card->id);
$subscription = $customer->subscriptions->add($subscriptionDataRequest);



} catch (OpenpayApiTransactionError $e) {
   $mensaje=error('ERROR en la transacción: ' . $e->getMessage());

} catch (OpenpayApiRequestError $e) {
	$mensaje=error('ERROR en la petición: ' . $e->getMessage());

} catch (OpenpayApiConnectionError $e) {
	$mensaje=error('ERROR en la conexión al API: ' . $e->getMessage());

} catch (OpenpayApiAuthError $e) {
	$mensaje=error('ERROR en la autenticación: ' . $e->getMessage());
	
} catch (OpenpayApiError $e) {
	$mensaje=error('ERROR en el API: ' . $e->getMessage());
	
} catch (Exception $e) {
	$mensaje=error('Error en el script: ' . $e->getMessage());
}


	if ($subscription->id) {
		$insertar_pago="INSERT INTO pagos (id_usuario, periodo_inicio, periodo_vencimiento, status, tipo_pago, fecha_pago, ip) VALUES ('".$id_usuario."', '".$periodo."', DATE_ADD('".$periodo_prox."', INTERVAL 7 DAY), '1', '".$suscription."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_pago=$base_datos->query($insertar_pago);
		$last_cc=substr($cc, -4);
		$actualizar_usuario="UPDATE usuarios SET cc='".$last_cc."', status='1' WHERE id_usuario='".$id_usuario."'";
		$actualizando_usuario=$base_datos->query($actualizar_usuario);
 
		$mensaje['app_status']=1;
		$mensaje['aviso']="Pago realizado satisfactoriamente"; }  
		return $mensaje;
	} 

?>