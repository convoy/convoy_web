<?php 

?>
<link rel="stylesheet" href="http://convoynetwork.com/convoy-webapp/css/style.css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" media="(max-width: 990px)" href="http://convoynetwork.com/convoy-webapp/css/desktop-990.css" />
<link rel="stylesheet" media="(max-width: 768px)" href="http://convoynetwork.com/convoy-webapp/css/tablet.css" />
<link rel="stylesheet" media="(max-width: 320px)" href="http://convoynetwork.com/convoy-webapp/css/movil.css" />



  <div class="podcastsPlayContenedor">
    <div class="podcastsPlay">
    <div class="btnRedesPodcasts">
     <ul class="btnRedesPodcastsCtn">
       <li><a href="javascript:void(0);" data-toggle="modal" data-target="#share<?php echo $_GET['id_podcast']; ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       	 width="60px" height="60px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
       <path fill="#FFFFFF" d="M42.014,35.719c-1.744,0-3.324,0.668-4.521,1.753L24.63,30.913c0.052-0.334,0.085-0.676,0.085-1.025
       	c0-0.198-0.013-0.39-0.031-0.583l12.988-6.618c1.174,0.991,2.686,1.592,4.342,1.592c3.715,0,6.727-3.011,6.727-6.726
       	c0-3.715-3.012-6.729-6.727-6.729s-6.729,3.011-6.729,6.729c0,0.205,0.012,0.408,0.031,0.608l-12.968,6.611
       	c-1.173-1.001-2.695-1.609-4.358-1.609c-3.715,0-6.729,3.011-6.729,6.729c0,3.714,3.015,6.728,6.729,6.728
       	c1.499,0,2.878-0.495,3.994-1.324l13.315,6.791c-0.004,0.119-0.018,0.238-0.018,0.361c0,3.715,3.012,6.729,6.729,6.729
       	c3.715,0,6.727-3.012,6.727-6.729C48.74,38.729,45.729,35.719,42.014,35.719z"/>
       </svg></a></li>

       <li  title="Guardar" data-toggle="modal" data-target="#myModal_pod<?php echo $_GET['id_podcast']; ?>"><a href="javascript:void(0);"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       	 width="60px" height="60px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
         
       <polygon fill="#FFFFFF" points="35.347,11.977 35.347,35.17 13.938,35.17 13.938,14.831 23.215,14.831 23.215,24.821 17.848,24.821 24.5,32.193
       	31.983,24.821 26.784,24.821 26.784,11.977 11.084,11.977 11.084,38.023 38.915,38.023 38.915,11.977 "/>
       </svg>
</a></li>
<script type="text/javascript">$('#Layer_1').click(function() { 
$('#path1').css({ fill: "#0f85fa" }); 
$('#path1').css({ fill: "#000" });
});

</script>
       <li onclick="like('podcast', <?php echo $_GET['id_podcast']; ?>);"><a href="javascript:void(0);"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       	 width="60px" height="60px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
         <?php 
          $seleccionar_like="SELECT * FROM usuarios_likes WHERE id_usuario='".$_SESSION['id_usuario']."' AND elemento='id_podcast=".$_GET['id_podcast']."'";
         $seleccionando_like=$base_datos->query($seleccionar_like);
         $num_like=$seleccionando_like->num_rows;
         ?>
       <path id="path1" fill="<?php if ($num_like==0) { ?>#FFFFFF<?php } else { ?>#0f85fa<?php } ?>" d="M30,46.439c1.417-1.418,9.512-9.516,14.571-14.574c4.532-4.533,4.436-11.141,0.314-15.24
       	C40.765,12.523,34.102,12.543,30,16.664c-4.101-4.121-10.764-4.141-14.887-0.039c-4.121,4.098-4.217,10.707,0.316,15.24
       	C20.489,36.924,28.583,45.021,30,46.439z"/>
       </svg>
</a></li>
     </ul>

    </div>

  <div id="audio-image" class="podcatsPlayImg">
    <img class="cover" class="img-responsive" width="100%" style="width:100%;"   />
  </div>
  <div id="audio-player">
    <!--<div id="audio-info">
      <span class="artist"></span><br />
      <span class="title"></span>
    </div>-->
    <div class="bgaudio"></div>
    <input id="progress" type="range">
   <br>
    <div id="tracker">

     <span id="duration"></span>
     <span id="durar" style="margin-top: 0px"> <?php echo str_replace('00:', '', $fila_track['duracion']); ?>0:00</span>
    </div>
    <!--<div id="progressBar">
      <span id="progress"></span>

    </div>-->
     <!--<input id="volume" type="range" min="0" max="10" value="5" />-->

     <div id="buttons" class="podcastsPlayControls">
         <div id="btnAudio">

      <button id="prev">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 width="40px" height="40px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
        <g>
        	<polygon fill="#FFFFFF" points="56.585,16.349 56.585,43.65 30,30.002 	"/>
        	<polygon fill="#FFFFFF" points="30,16.349 30,43.65 3.415,30.002 	"/>
        </g>
        </svg>



      </button>
      <button id="play"><svg version="1.1" id="PlayIcono" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="40px" height="40px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
      <g>
        <polygon fill="#FFFFFF" points="3.109,15.307 3.109,4.693 16.891,9.999 	"/>
      </g>
      </svg></button>
    <button id="pause" style="display: none !important"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    	 width="40px" height="40px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
    <g>
    	<rect fill="#FFFFFF" x="4.833" y="3.806" width="3.604" height="12.387"/>
    	<rect fill="#FFFFFF" x="11.563" y="3.806" width="3.604" height="12.387"/>
    </g>
    </svg>
</button>
      <!--<button id="stop">stop</button>-->
      <button id="next"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      	 width="40px" height="40px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
      <g>
      	<polygon fill="#FFFFFF" points="3.415,43.65 3.415,16.349 30,29.998 	"/>
      	<polygon fill="#FFFFFF" points="30,43.65 30,16.349 56.585,29.998 	"/>
      </g>
      </svg></button>

     </div>
   </div>


   </div>
 </div>

     <div class="podcasts-season"><!--inicia el podcasts-->
        <div class="play-season">
     <ul id="playlist" class="show">
    <div class="scroll" style="overflow-y: scroll; height: 717px;">
     <?php
     $seleccionar_podcast="SELECT * FROM podcast WHERE id_podcast='".$_GET['id_podcast']."'";
     $seleccionando_podcast=$base_datos->query($seleccionar_podcast);
     $fila_podcast=$seleccionando_podcast->fetch_array();
     $seleccionar_track="SELECT * FROM podcast_track WHERE id_podcast='".$_GET['id_podcast']."'";
     $seleccionando_track=$base_datos->query($seleccionar_track);
$pop=0;
     while($fila_track=$seleccionando_track->fetch_array()) {
$pp
     ?>
      <li song="<?php echo $fila_track['url_track']; ?>" cover="<?php echo $fila_podcast['url_imagen']; ?>" artist="<?php echo utf8_encode($fila_podcast['titulo']); ?>" durac="<?php echo str_replace('00:', '', $fila_track['duracion']); ?>" titulo="Episodio <?php echo utf8_encode($fila_track['episodio']); ?>" descripcion="<?php echo utf8_encode($fila_track['descripcion']); ?>">
      <span class="tituloPodcastsBase"><?php echo utf8_encode($fila_track['titulo']); ?></span>
          <span class="infoBtn" style="cursor: normal; margin-left: 100px"><i class="fa fa-info-circle fa-3 info-pods" ></i>   <span > <?php echo str_replace('00:', '', $fila_track['duracion']); ?></span></span>
      </li>
    <?php } ?>
  </div>

    </ul>
  </div>

  </div><!--Termina el podcasts-->
  <div class="podcasts-season">
      <div class="podcastsPlayTexto">
        <h3 class="text-uppercase"><?php echo $fila_podcast['titulo']; ?></h3>
        <p>Temporada <?php echo $fila_podcast['temporada'] ?></p>
        <p class="text-justify">
          <?php echo $fila_podcast['descripcion']; ?>. </p>
      </div>



  </div>
  <!--Info pop-->


  <div id="Noticias1" class="muestra-noti"><!--inicia el podcasts-->
     <div class="info-season"><!--imagen-->
       <i class="fa fa-info-circle fa-3"></i><i id="closeInfoPodcasts" style="float: right;" class="fa fa-close"></i>
      <h3 class="text-uppercase" id="podcast_track_titulo">Podcasts title 1</h3>
       <p class="text-justify" id="podcast_track_descripcion">
         Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
          nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
          fugiat nulla pariatur. Excepteur sint
          occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
       </p>
     </div>
  </div><!--Termina el podcasts-->
 

<div class="modal fade" id="myModal_pod<?php echo $_GET['id_podcast']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content personalModal">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Guardar podcast</h3>

      </div>
      <div class="modal-body">

            <h1 class="text-center text-uppercase"><?php echo utf8_encode($fila_noticia['cabeza']); ?></h1>
            <div id="guardar_elemento_tal_<?php echo $fila_noticia['id_noticia']; ?>">
            <h2 class="text-center text-uppercase">Escoger carpeta</h2>
            <ul class="activity-osb text-uppercase"><?php
           $seleccionar_carpetas="SELECT * FROM usuarios_carpetas WHERE id_usuario='".$_SESSION['id_usuario']."'";
            $seleccionando_carpetas=$base_datos->query($seleccionar_carpetas);
            while($fila_carpetas=$seleccionando_carpetas->fetch_array()) {
            ?>
              <li><a class="btn btnSave" href="javascript:guardar_elemento('noticia', '<?php echo $fila_noticia['id_noticia']; ?>', '<?php echo $fila_carpetas['id_carpeta']; ?>');"><?php echo $fila_carpetas['nombre_carpeta']; ?></a></li>
              <?php

               } /* ?>
              <li class="kas"><a href="#">+ Guardar carpeta</a></li> */ ?>
            </ul>
            </div>

      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
<div class="modal fade" id="share<?php echo $_GET['id_podcast']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content personalModal">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Compartir</h3>

      </div>
      <div class="modal-body">

            <h1 class="text-center text-uppercase"><?php echo $fila_noticia['cabeza']; ?></h1>

            <ul class="activity-share text-uppercase">
              <li><a href="#">

                  <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" id="circuloShare" d="M37.828,20c0,9.845-7.981,17.828-17.828,17.828c-9.846,0-17.828-7.982-17.828-17.828
    c0-9.846,7.982-17.828,17.828-17.828C29.847,2.172,37.828,10.154,37.828,20"/>
  <path fill="#FFFFFF" d="M33.292,17.174c-1.104-1.505-24.813-2.046-26.977,0c0,0-1.324,2.965,0,5.804c0,0,1.275,0.297,3.563,0.598
    l3.581,7.25v-6.875c1.867,0.148,4.048,0.256,6.506,0.256c8.392,0,13.326-1.229,13.326-1.229
    C34.617,20.139,33.292,17.174,33.292,17.174z"/>
</g>
</svg>

              </a></li>
              <li><a href="#">
                <svg class="iconosShare"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path id="circulo2" class="circuloShare" fill="#B3B3B3" d="M38.537,20c0,10.238-8.299,18.537-18.537,18.537C9.763,38.537,1.463,30.238,1.463,20S9.763,1.463,20,1.463
    C30.238,1.463,38.537,9.763,38.537,20"/>
  <g>
    <polygon fill="#FFFFFF" points="20,23.029 8.371,14.758 8.371,27.443 31.63,27.443 31.63,14.758     "/>
    <polygon fill="#FFFFFF" points="31.512,12.557 8.488,12.557 20,20.72     "/>
  </g>
</g>
</svg>

              </a></li>
              <li><a href="https://plus.google.com/share?url=http%3A//convoynetwork.com/convoy-webapp/podcasts?id_podcast=<?php echo $_GET['id_podcast']; ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.828,20c0,10.398-8.429,18.828-18.828,18.828C9.602,38.828,1.172,30.398,1.172,20S9.602,1.172,20,1.172
    C30.399,1.172,38.828,9.602,38.828,20"/>
  <g>
    <polygon fill="#FFFFFF" points="32.209,13.271 28.559,13.271 28.559,9.012 27.343,9.012 27.343,9.086 27.343,13.271
      23.084,13.271 23.084,15.095 27.343,15.095 27.343,18.745 28.559,18.745 28.559,15.095 32.209,15.095     "/>
    <path fill="#FFFFFF" d="M18.438,21.606c-0.595-0.421-1.731-1.445-1.731-2.046c0-0.705,0.201-1.053,1.263-1.881
      c1.087-0.85,1.856-1.849,1.856-3.239c0-1.654-0.736-3.603-2.119-3.603h2.085l1.471-1.825h-6.575c-2.948,0-5.723,2.419-5.723,5.006
      c0,2.644,2.01,4.871,5.009,4.871c0.208,0,0.411,0.042,0.609,0.028c-0.194,0.373-0.333,0.816-0.333,1.251
      c0,0.735,0.396,1.342,0.895,1.828c-0.378,0-0.742,0.017-1.14,0.017c-3.651,0-6.462,2.328-6.462,4.74
      c0,2.375,3.081,3.862,6.732,3.862c4.163,0,6.463-2.361,6.463-4.736C20.738,23.974,20.176,22.834,18.438,21.606 M14.926,18.311
      c-1.695-0.05-3.306-1.894-3.597-4.119c-0.293-2.226,0.843-3.928,2.537-3.877c1.693,0.051,3.304,1.835,3.597,4.06
      C17.756,16.6,16.618,18.362,14.926,18.311 M14.262,29.566c-2.523,0-4.347-1.598-4.347-3.517c0-1.881,2.261-3.447,4.785-3.42
      c0.589,0.007,1.138,0.102,1.636,0.263c1.371,0.952,2.353,1.491,2.63,2.577c0.053,0.221,0.08,0.446,0.08,0.678
      C19.046,28.066,17.811,29.566,14.262,29.566"/>
  </g>
</g>
</svg>
              </a></li>
              <li class=""><a href="https://twitter.com/home?status=http%3A//convoynetwork.com/convoy-webapp/podcasts?id_podcast=<?php echo $_GET['id_podcast']; ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.453,20c0,10.191-8.262,18.452-18.453,18.452C9.809,38.452,1.547,30.191,1.547,20
    C1.547,9.809,9.809,1.548,20,1.548C30.191,1.548,38.453,9.809,38.453,20"/>
  <path fill="#FFFFFF" d="M32.605,12.668c-0.891,0.396-1.848,0.663-2.854,0.783c1.025-0.615,1.814-1.589,2.186-2.75
    c-0.961,0.569-2.023,0.982-3.156,1.206c-0.906-0.966-2.197-1.569-3.625-1.569c-2.746,0-4.969,2.225-4.969,4.968
    c0,0.39,0.043,0.769,0.128,1.133c-4.129-0.207-7.791-2.186-10.241-5.192c-0.427,0.735-0.673,1.588-0.673,2.499
    c0,1.724,0.877,3.244,2.21,4.135c-0.814-0.025-1.581-0.249-2.25-0.621C9.36,17.281,9.36,17.303,9.36,17.324
    c0,2.407,1.713,4.415,3.986,4.872c-0.417,0.113-0.855,0.175-1.309,0.175c-0.32,0-0.631-0.032-0.935-0.09
    c0.632,1.974,2.467,3.41,4.641,3.451c-1.7,1.333-3.842,2.127-6.17,2.127c-0.401,0-0.796-0.023-1.186-0.07
    c2.2,1.41,4.811,2.233,7.617,2.233c9.14,0,14.136-7.571,14.136-14.138c0-0.215-0.004-0.43-0.014-0.642
    C31.098,14.54,31.941,13.665,32.605,12.668"/>
</g>
</svg>
              </a></li>
              <li class=""><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//convoynetwork.com/convoy-webapp/podcasts?id_podcast=<?php echo $_GET['id_podcast']; ?><?php ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.953,20c0,10.467-8.485,18.953-18.953,18.953S1.047,30.467,1.047,20S9.532,1.047,20,1.047
    S38.953,9.533,38.953,20"/>
  <path fill="#FFFFFF" d="M9.3,16.643c0,0-1.111-0.31-1.111,1.111l0.662,9.188c0,0,0.111,0.9,0.844,0.9h6.612
    c0,0,0.761,0.084,0.761-1.379c0,0,0.393,0.646,1.913,0.646h8.554c0,0,2.278-0.028,2.278-2.278c0,0,0.015-0.746-0.309-1.07
    c0,0,0.957-0.506,0.957-1.435c0,0,0.14-0.618-0.282-1.042c0,0,1.407-0.843,0.704-2.447c0,0,0.929-0.45,0.929-1.829
    c0,0,0-1.802-1.802-1.802h-5.909c0,0,0.788-2.701,0.281-4.333c0,0-0.899-3.292-2.897-3.292c0,0-2.054,0.225-1.379,2.307
    c0,0,0.675,2.026-0.731,3.798l-2.307,3.406c0,0,0.112-0.451-0.677-0.451H9.3z"/>
</g>
</svg>

              </a></li>

            </ul>
            <input  class="text-uppercase form-control" type="button" placeholder="Aceptar"  data-dismiss="modal" aria-label="Close" value="Copiar" style="cursor: pointer">

      </div>

    </div>
  </div>
</div>
 <script src="http://code.jquery.com/jquery-1.11.3.js"></script>
 <script src="http://convoynetwork.com/convoy-webapp/audio-archivo/audio/js/main.js"></script>

 <script type="text/javascript" src="http://convoynetwork.com/convoy-webapp/js/app-tabs.js"></script>
 
