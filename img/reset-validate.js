function validate(){
		
    var Msg = "";
        
    var password1 = $('input[name=NewPass]').val().trim();
    var password2 = $('input[name=NewPassRepeat]').val().trim();
    var email = $('input[name=email]').val().trim();
    var token = $('input[name=token]').val().trim();
    
    
    if(password1 != password2){
        alert('Las contraseñas no coinciden');
        return false;
    }else{
    
        $.ajax({
               type: "POST",
               headers: { 'Apikey': 'ac2ee9536910fa6cea18ebaf294467a8' },
               url: '../ws/password-reset.php',
               data: {'Email':email,'NewPass':password1,'Token':token},
               success: function(){alert("Tu contraseña se ha reestablecido exitosamente");},
               error: function(){alert("Hubieron problemas al reestablecer tu contraseña. Intenta nuevamente.");}
               });
    }
    


}
