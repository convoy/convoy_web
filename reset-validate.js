function validate(){
		
    var Msg = "";
        
    var password1 = $('input[name=NewPass]').val().trim();
    var password2 = $('input[name=NewPassRepeat]').val().trim();
    var email = $('input[name=email]').val().trim();
    var token = $('input[name=token]').val().trim();
    
    var data = {
        'Email':email,
        'NewPass': password1,
        'Token':token
    }
    
    if(password1 != password2){
        alert('Las contraseñas no coinciden');
        return false;
    }else{
    
        $.ajax({
               type: "POST",
               dataType:"json",
               headers: {'Apikey':'ac2ee9536910fa6cea18ebaf294467a8'},
               url: baseUrl + '/ws/password-reset-web.php',
               data: data,
               success: function(result){
                alert("Tu contraseña se ha reestablecido exitosamente"+result);
               },
               error: function(result){
                    alert("Hubieron problemas al reestablecer tu contraseña. Intenta nuevamente."+result);
               }
               });
    }
    


}
