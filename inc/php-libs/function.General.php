<?php

include __DIR__ . '/../../base_url.php';

function MonthText($string){
	switch($string){
		case '01': $Month = 'Enero'; break;
		case '02': $Month = 'Febrero'; break;
		case '03': $Month = 'Marzo'; break;
		case '04': $Month = 'Abril'; break;
		case '05': $Month = 'Mayo'; break;
		case '06': $Month = 'Junio'; break;
		case '07': $Month = 'Julio'; break;
		case '08': $Month = 'Agosto'; break;
		case '09': $Month = 'Septiembre'; break;
		case '10': $Month = 'Octubre'; break;
		case '11': $Month = 'Noviembre'; break;
		case '12': $Month = 'Diciembre'; break;
		}
	return $Month;
	}

function DateFront($string) {
	list($a, $m, $d) = explode('-', $string);
	return MonthText($m).' '.$d.', '.$a;
	}


function Brief($string, $limitchars){
	$string = strip_tags($string);
	if (strlen($string) > $limitchars) {
		// truncate string
		$stringCut = substr($string, 0, $limitchars);
    // make sure it ends in a word so assassinate doesn't become ass...
		$string = substr($stringCut, 0, strrpos($stringCut, ' '));
		$string.='...';
		}
	return $string;
	}


function SwitchMime($Mime){
	switch($Mime){
		case 'image/gif': $ext = 'gif'; break;
		case 'image/jpeg': $ext = 'jpg'; break;
		case 'image/jpg': $ext = 'jpg'; break;
		case 'image/png': $ext = 'png'; break;
		}
	return $ext;
	}

?>