<?php 
$DB=ConnDB();
?> 
<!--Actividad-->
<ul class="activity">
<?php
$s = "(
SELECT
'N' AS Type,
news_NewsID AS ID,
news_Title AS Title,
news_Brief AS Brief,
news_Datetime AS Datetime
FROM News WHERE news_Datetime BETWEEN '2015-12-29 00:00:00' AND NOW()
)
UNION
(
SELECT
'R' AS Type,
radi_RadioID AS ID,
radi_Title AS Title,
radi_Descripcion AS Brief,
radi_IniDatetime AS Datetime
FROM Radio WHERE radi_IniDatetime BETWEEN '2015-12-29 00:00:00' AND NOW()
)
UNION
(
SELECT
'P' AS Type,
epis_EpisodeID AS ID,
CONCAT('Episodio ',epis_Episode,' - ',epis_Title) AS Title,
epis_Description AS Description,
epis_PlayDate AS Datetime 
FROM Podcast_Episodes WHERE epis_PlayDate BETWEEN '2015-12-29 00:00:00' AND NOW()
)
ORDER BY Datetime DESC";
$q = $DB->query($s);
while($r=$q->fetch_array()) {
?>
	<li class="fol"> <a href="#"><?php echo $fila_podcast['titulo']; ?> </a><i class="iconPodcasts iconPodcastsCorazon"></i></li>
<?php
	}
?>
</ul>