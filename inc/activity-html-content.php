<?php 
if(!isset($_SESSION)){
	session_start();
//	require_once('../php-libs/function.SessionValidate.php');
	}
if(!isset($DB)){
	require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
	}
?>
<ul class="activity">
<?php

function AlertIcon($string){
	switch($string){
		case 'like':
			$icon = 'iconPodcastsCorazon';
			break;
		case 'follower':
			$icon = 'iconPodcastsPersonaAzul';
			break;
		default:
			$icon = '';
			break;
		}
	}



$DateCreation = date('Y-m-d');
$DateLimit = date('Y-m-d', strtotime($DateCreation. ' - 1 days'));



$sF = "SELECT foll_UserID2 FROM Followers WHERE foll_UserID1='".$_SESSION['SessionUserID']."'";
$qF = $DB->query($sF);
$Following = '';
while($rF = $qF->fetch_array()){
	$Following.= $rF['foll_UserID2'].', ';
	}

$Following = substr_replace($Following, "", -2);


$sA = "
(
SELECT
'N' AS Type,
news_NewsID AS ID,
CONCAT('<a href=\"".BASE_URL."/convoy-webapp/noticias?NewsID=',news_NewsId,'\">',news_Title,'</a>') AS Title,
news_Datetime AS Datetime
FROM News WHERE news_Datetime BETWEEN '".$DateLimit." 00:00:00' AND NOW()
)
UNION
(
SELECT
'R' AS Type,
radi_RadioID AS ID,
CONCAT('<a href=\"".BASE_URL."/convoy-webapp/radio\">',radi_Title,'</a>') AS Title,
radi_IniDatetime AS Datetime
FROM Radio WHERE radi_IniDatetime BETWEEN '".$DateLimit." 00:00:00' AND NOW()
)
UNION
(
SELECT
'P' AS Type,
epis_EpisodeID AS ID,
CONCAT('Episodio ',epis_Episode,' - ',epis_Title) AS Title,
epis_PlayDate AS Datetime 
FROM Podcast_Episodes WHERE epis_PlayDate BETWEEN '".$DateLimit." 00:00:00' AND NOW()
)
";
if($Following!=''){
	$sA.="
		UNION
		(
		SELECT 
		'F' AS Type,
		'' AS ID,
		CONCAT('<a href=\"".BASE_URL."/convoy-webapp/users-info?ID=',foll_UserID1,'\">',a.user_Login,'</a> ahora sigue a <a href=\"".BASE_URL."/convoy-webapp/users-info?ID=',foll_UserID2,'\">',b.user_Login,'</a>') AS Title,
		foll_Datetime AS Datetime
		FROM Followers
		INNER JOIN Users AS a ON foll_UserID1=a.user_Userid
		INNER JOIN Users AS b ON foll_UserID2=b.user_Userid
		
		WHERE foll_Datetime BETWEEN '2015-12-29 00:00:00' AND NOW()
		AND foll_UserID1 IN (".$Following.")
		)
	";
	}
$sA.="ORDER BY Datetime DESC
";

$qA = $DB->query($sA);
while($rA = $qA->fetch_array()){
	echo '<li class="fol"><i class="iconPodcasts"></i>'.$rA['Title'].'</li>';
	}
?>
    </ul>
