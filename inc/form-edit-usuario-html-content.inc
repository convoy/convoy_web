<?php
$s = "
	SELECT
		user_Userid,
		user_Login,
		user_Email,
		user_FullName,
		user_Genre,
		user_Birthdate,
		user_Avatar,
		user_Description,
		user_Website,
		user_ProfileType,
		user_Instagram,
		user_Twitter,
		user_Facebook
	FROM Users WHERE user_Userid='".$_SESSION['SessionUserID']."'";
$q = $DB->query($s);
$r = $q->fetch_array();
if($r['user_Avatar']!=''){
	$FormAvatar = BASE_URL.'/convoy-webapp/image-avatar/'.$r['user_Userid'].'/'.$r['user_Avatar'];
	}
else{
	$FormAvatar = BASE_URL.'/convoy-webapp/image-avatar/avatar.png';
	}

?>
<div id="resultado_perfil">
  <div class="EditarPerfil" id="content_per">
    <form class="EditarPerfilForm" action="php-scripts/user-update.php" method="post" enctype="multipart/form-data">
      <div class="EditarPerfilUsuario">
      	<!--<div class="form-control formPersonal">-->
        <input type="text" class="form-control formPersonal" placeholder="Sitio Web" name="user_Login"  value="<?php echo $r['user_Login']; ?>" readonly />
        <!--</div>-->
        </div>
        <div class="EditarPerfilFoto">
        <input type="file" id="subirFoto" name="userfile"><label for="subirFoto" id="foto_id" >
        <img id="subirFotoInt" src="<?php echo $FormAvatar; ?>"  style="width: 100px; height: auto" /><br />Subir foto de perfil</label>
        <textarea name="user_Description" class="form-control formPersonal" placeholder="Descripción introduce el texto aqui, hasta 120 caracteres"  maxlength="120"><?php echo $r['user_Description']; ?></textarea>
        </div>
        <div class="EditarPerfilInputs">
        <input type="text" class="form-control formPersonal" placeholder="Sitio Web" name="user_Website"  value="<?php echo $r['user_Website']; ?>">
        <input type="text" class="form-control formPersonal" placeholder="Instragram" name="user_Instagram" value="<?php echo $r['user_Instagram']; ?>">
        <input type="text" class="form-control formPersonal" placeholder="Facebook" name="user_Facebook" value="<?php echo $r['user_Facebook']; ?>">
        <input type="text" class="form-control formPersonal" placeholder="Twitter" name="user_Twitter" value="<?php echo $r['user_Twitter']; ?>"
        </div>
        <div class="EditarPerfilCheckbox">
        <input type="radio" id="Privado" name="user_ProfileType" value="0"<?php if($r['user_ProfileType']==0) { echo ' checked';} ?>><label for="Privado" id="D3">Privado</label>
        <input type="radio" id="Publico" name="user_ProfileType" value="1"<?php if($r['user_ProfileType']==1) { echo ' checked';} ?>><label for="Publico" id="D4">Público</label>
        </div>
        <button class="btn EditarPerfilBtn" type="submit">Guardar</button>
      </form>
  </div>
</div>
