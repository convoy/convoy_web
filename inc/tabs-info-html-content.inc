<section class="ContenidoTabsUsuario heightPersonal">
<?php
$s = "
SELECT 
user_Userid,
user_Description,
user_Website,
user_Instagram,
user_Facebook,
user_Twitter,
(SELECT COUNT(*) FROM Followers WHERE foll_UserID1=user_Userid) AS Following,
(SELECT COUNT(*) FROM Followers WHERE foll_UserID2=user_Userid) AS Followers
FROM Users WHERE user_Userid='".$_SESSION['SessionUserID']."'";
$q = $DB->query($s);
$r = $q->fetch_array();
?>

 <p class="ContenidoTexto"><?php echo $r['user_Description']; ?></p>
<ul class="IconLinkNav">

<?php
if($r['user_Website']!=''){
    $LinkWebsite= "http://".$r['user_Website'];
    echo '<a href="'.$LinkWebsite.'" title="Website" target="_blank"><img  class="ImgLinkIconos" src="'.BASE_URL.'/convoy-webapp/img/iconos/link.png" alt="Website"></a>';
} else {
    echo '<img class="ImgLinkIconos noLink" src="'.BASE_URL.'/convoy-webapp/img/iconos/link.png" alt="Website">';
}
if($r['user_Instagram']!=''){
    $LinkInstagram= "https://instagram.com/".$r['user_Instagram'];
    echo '<a href="'.$LinkInstagram.'" title="Instagram" target="_blank"> <img class="ImgLinkIconos"  src="'.BASE_URL.'/convoy-webapp/img/iconos/instagram.png" alt="Instagram"></a>';
} else {
    echo '<img class="ImgLinkIconos noLink"  src="'.BASE_URL.'/convoy-webapp/img/iconos/instagram.png" alt="Instagram">';
}
if($r['user_Facebook']!=''){
    $LinkFacebook= "https://facebook.com/".$r['user_Facebook'];
    echo '<a href="'.$LinkFacebook.'" title="Facebook" target="_blank"> <img class="ImgLinkIconos"  src="'.BASE_URL.'/convoy-webapp/img/iconos/facebook.png" alt="Facebook"></a>';
} else {
    echo '<img class="ImgLinkIconos noLink"  src="'.BASE_URL.'/convoy-webapp/img/iconos/facebook.png" alt="Facebook">';
    
}
if($r['user_Twitter']!=''){
    $LinkTwitter= "https://twitter.com/".$r['user_Twitter'];
    echo '<a href="'.$LinkTwitter.'" title="Twitter" target="_blank"> <img class="ImgLinkIconos"  src="'.BASE_URL.'/convoy-webapp/img/iconos/twitter.png" alt="Twitter"></a>';
} else {
    echo '<img class="ImgLinkIconos noLink"  src="'.BASE_URL.'/convoy-webapp/img/iconos/twitter.png" alt="Twitter">';
}
?>

<a title="link" id="Followers" class="seguidores text-upprecase"> Seguidores <br><?php echo $r['Followers'] ?></a>
<a title="link" id="Following" class="seguidores text-upprecase"> Siguiendo <br><?php echo $r['Following'] ?></a>
</ul>
 <div id="tabs" class="tabs-info-2">
    <ul class="tabs">
      <li class="activa">
      <a class="text-uppercase tabsTitulos" id="Activities">
      <svg class="iconsMenuTabs" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
<path d="M43.314,24.911h-4.463L34.633,13.19c-0.248-0.743-0.99-1.241-1.734-1.241c-0.742,0-1.428,0.498-1.672,1.241l-5.954,18.914
	l-6.014-13.582c-0.249-0.62-0.807-0.991-1.489-1.053c-0.682-0.061-1.302,0.249-1.673,0.807l-4.402,6.635H6.356
	c-0.992,0-1.8,0.807-1.8,1.799s0.808,1.799,1.8,1.799h6.262c0.62,0,1.179-0.31,1.489-0.807l3.162-4.713l6.635,14.945
	c0.31,0.682,0.93,1.053,1.611,1.053h0.123c0.743-0.062,1.363-0.559,1.612-1.24l5.771-18.355l2.852,7.874
	c0.249,0.743,0.932,1.179,1.674,1.179h5.704c0.991,0,1.8-0.807,1.8-1.799C45.114,25.715,44.307,24.911,43.314,24.911"/>
</svg>
     <span>Actividad</span> </a></li>
<!--
      <li>
      <a class="text-uppercase tabsTitulos" id="Collections">
      <svg class="iconsMenuTabs" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
<polygon points="35.347,11.977 35.347,35.17 13.938,35.17 13.938,14.831 23.215,14.831 23.215,24.821 17.848,24.821 24.5,32.193
	31.983,24.821 26.784,24.821 26.784,11.977 11.084,11.977 11.084,38.023 38.915,38.023 38.915,11.977 "/>
</svg>
 <span>Colecciones</span></a></li>
      <li>
      <a class="text-uppercase tabsTitulos" id="Messages">
      <svg class="iconsMenuTabs" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
<g>
	<path d="M31.399,22.591H5.39c-1.023,0-1.854,0.83-1.854,1.854v5.659c0,1.023,0.83,1.854,1.854,1.854h3.582l5.145,5.436v-5.436
		h17.287c1.023,0,1.854-0.83,1.854-1.854v-5.659C33.254,23.421,32.424,22.591,31.399,22.591"/>
	<path d="M44.813,11.493H18.8c-1.023,0-1.854,0.83-1.854,1.854v5.66c0,1.022,0.83,1.854,1.854,1.854h17.283v5.436l5.146-5.436h3.582
		c1.022,0,1.854-0.831,1.854-1.854v-5.66C46.665,12.32,45.838,11.493,44.813,11.493"/>
</g>
</svg>  <span>Mensajes</span></a></li>-->

    </ul>

    <div id="TabContent" class="contenidoTab Interior">
     <?php  require_once('activity-html-content.php');?>
   </div>



  </div><!--Termina todos los tabas-->

  <div id="tab-5" class="contenidoTab"><!--Seguidores-->
 <ul class="activity sin-padding">
    <li class="fol"><a href=""><img src="<?php echo BASE_URL;?>/convoy-webapp/image-avatar/defaultConvoy.jpg" class="img-responsive" alt="" /><span><i class="iconPodcasts iconPodcastsPersonaAzul"></i><i class="iconPodcasts iconPodcastsPersonaGris"></i><a href="javascript:cambio_tabla('#tab-3')"><img src="https://convoynetwork.com/convoy-webapp/img/iconos/mensajes-azul.svg" style="margin-left: 120px"></a></a></li>

 </ul>
</div><!--Seguidores-->

     <div id="tab-6" class="contenidoTab">
      <!--siguiendo--><ul class="activity sin-padding">
    <li class="fol"><a href="#"><img src="<?php echo BASE_URL;?>/convoy-webapp/image-avatar/defaultConvoy.jpg" class="img-responsive" alt="" /><i class="iconPodcasts iconPodcastsPersonaAzul"></i><i class="iconPodcasts iconPodcastsPersonaGris"></i></a></li>


 </ul>
</div><!--Siguiendo-->


</section>
