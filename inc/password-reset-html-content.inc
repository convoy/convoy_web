<?php
if (isset($_GET['token']))
    $token = $_GET['token'];

if (isset($_GET['email']))
    $email = $_GET['email'];
?>
<div class="container">
				<div class="row">
							<div class="col-sm-12 col-md-6 col-md-offset-3 col-lx-8"><!--Comineza la columna-->
							 <h1 class="text-center"><img class="logoRegistro" src="img/logo-convoy.png"></h1>
							<form class="form" method="post" onsubmit="return validate(this);">



						    <h2 class="text-center up-letter">Reestablecer contraseña</h2>
								<div class="col-md-6 col-md-offset-3 col-sm-12 col-lx-6">
									 <input type="password" class="form-control form-per" name="NewPass" placeholder="Contraseña Nueva" required autocomplete="off">
								</div>

								<div class="col-md-6 col-md-offset-3 col-sm-12 col-lx-6">
									 <input type="password" class="form-control form-per" name="NewPassRepeat" placeholder="Repetir contraseña" required autocomplete="off">
								</div>

								<div class="col-md-12 col-sm-12 whiteA">
                                <input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
                                <input type="hidden" name="email" id="email" value="<?php echo $email; ?>">
                                <button id="resetPassword" type="submit" class="btn btn-block">Reestablecer contraseña</button>

							 </div>
							</form>
<p class="text-center" style="clear:both;display:block; margin-top:20px;">Ir a: <a href="login" data-toogle="modal" data-target="#olvido" title="login">Inicio de sesión</a></p>

						</div>

						<!--Fin de la columna-->
                        <script type="text/javascript" src="js/reset-validate.js"></script>
				</div>
</div>
