<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Saving</h3>

      </div>
      <div class="modal-body">

            <h1 class="text-center text-uppercase">"Welcome To Convoy"</h1>
            <h2 class="text-center text-uppercase">Choose Folder</h2>
            <ul class="activity-osb text-uppercase">
              <li><a href="#">My Favorite Podcasts</a></li>
              <li><a href="#">My Favorite Stories</a></li>
              <li><a href="#">Sound & Vision</a></li>
              <li class="kas"><a href="#">+ Create New Collections</a></li>
            </ul>

      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
