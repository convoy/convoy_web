
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3 col-lx-8"><!--Comineza la columna-->
            <h1 class="text-center"><img class="logoRegistro" src="img/logo-convoy.png"></h1>
                <!--form class="form" action="php-libs/login.php" method="post"-->
                <form class="form" method="post" onsubmit="return validate(this);">
                    <h2 class="text-center up-letter">Restaurar Contraseña</h2>
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-lx-6">
                         <input type="text" class="form-control form-per" name="usuario" placeholder="EMAIL" required autocomplete="off">
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <button id="startSession" type="submit" class="btn btn-block">Enviar</button>
                    </div>

                    <div class="text-center text-uppercase whiteA clearfix" >
                        <br>
                        ¿Ya Tienes una cuenta? <a href="#" data-dismiss="modal" aria-label="Close" class="ColorWhite" title="Iniciar sesión">Iniciar sesión</a>
                    </div>
                </form>
        </div>

        <!--Fin de la columna-->
        <script type="text/javascript" src="js/restore-validate.js"></script>

    </div>
</div>
