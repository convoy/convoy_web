<div id="BuscadorGeneral">

	<div class="FormBusqueda">

		<div class="BackBusqueda">

    <input type="hidden" name="uId" id="uId" value="<?php echo $_SESSION['SessionUserID'] ?>" />

    <input class="form-control form-per InputBusqueda" placeholder="Buscar" type="text" id="SearchTerm" name="SearchTerm">

    <button class="btn btnBusqueda" id="ButtonSearch">

    	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">

      	<g>

        	<path fill="#FFFFFF" d="M11.333,34.747H1.939l10.419-13.664L1.939,7.418h9.394l10.42,13.665L11.333,34.747z M28.755,34.747h-9.394 L29.78,21.083L19.361,7.418h9.394l10.419,13.665L28.755,34.747z"/>

        </g>

			</svg>

		</button> 

		</div>

	</div>

  <div class="ResultadosBusqueda" id="resultado_busqueda">

    <ul class="ResultadoBtn">

      <li class="ResultadoList activeBusqueda">

      <input type="radio" class="SearchFor" name="SearchFor" id="SearchContent" value="C" checked>
      <label class="ResultadoLink selectedSearchButton" for="SearchContent">Contenido</label>

      </li>

      

      <li class="ResultadoList">

      
      <input type="radio" class="SearchFor" name="SearchFor" id="SearchUsers" value="U">
      <label  class="ResultadoLink" for="SearchUsers">Usuario</label>

      </li>

    </ul>

  </div>

  <ul id="SearchResults" class="ResultadoLista bc-content" style="overflow-y: auto; height: 450px"></ul>
  <ul id="searchProfileResult" class="ajustesInfo Btnctn animated slideInLeft" style="left: 0px; transition: all 0.5s ease 0s;">
    <section class="ContenidoTabsUsuario heightPersonal" style="display:none;"></section>
  </ul>
</div>