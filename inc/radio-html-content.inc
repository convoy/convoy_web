<!--Navegación-->

<?php
require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
require_once(__DIR__ . '/../php-libs/function.General.php');
include 'nav.inc';
include 'tabs-welcome.inc';

date_default_timezone_set("America/Mexico_City");

if(isset($_GET['fecha_tiempo'])){
    $s = "SELECT radi_RadioID, radi_Title,radi_Image,radi_Track FROM Radio WHERE DATE(radi_IniDatetime)='".$_GET['fecha_tiempo']."' AND radi_Status='A' ORDER BY radi_IniDatetime ASC";
    $q = $DB->query($s);
    $t = $q->num_rows;
}else{
    //$s = "SELECT radi_RadioID FROM Radio WHERE radi_IniDatetime<=NOW() AND radi_EndDatetime>NOW()";
    $s = "SELECT radi_RadioID, radi_Title,radi_Image,radi_Track FROM Radio WHERE DATE(radi_IniDatetime)=DATE(DATE_SUB(NOW(), INTERVAL 6 HOUR)) AND radi_Status='A' ORDER BY radi_IniDatetime ASC";
    $q = $DB->query($s);
    $t = $q->num_rows;
}
?>
<!-- INFO BLOCK -->

<div id="LoaderInfo"></div>

<!-- //INFO BLOCK -->

<!--Estudio generalizado-->
<div class="" style="margin-top: 10em;">
<h2 class="text-center text-uppercase" style="color: white;">
<?php
$now = date("Y-m-d");
if(!isset($_GET['fecha_tiempo'])){
    //echo "Ahora escuchas";
}else{
    if($_GET['fecha_tiempo'] < $now){
        //echo "Programación pasada";
    }else if($_GET['fecha_tiempo'] > $now){
        //echo "Próxima programación";
    }else if($_GET['fecha_tiempo'] == $now){
        //echo "Ahora escuchas";
    }
}

?>
</h2>
  <div class="flipster" id="Mejor">
      <ul class="flip-items">
      
        <?php while($r = $q->fetch_array()) : ?>
        <li id="Coverflow" title="" data-flip-category="Fun Sports">
            <div class="audio_radio" id="<?php echo $r['radi_RadioID']; ?>" style="width: 100%; height: 1px; overflow: hidden; display:none;"></div>

        	<img id="RadioShowImage" src="<?php
            if($r['radi_Image']!==''){
                echo 'img/cover-radio/'.$r['radi_Image'];
            }else{
                echo 'img/cover-radio/logo-convoy.png';
            }
            ?>" style="cursor: pointer;" onerror="imgError(this);">
                <h3 class="FlipTitulo" id="RadioShowTitle"><?php echo $r['radi_Title']; ?></h3>
                <span id="Loader"></span>

        </li>
        <?php endwhile; ?>


      </ul>

    </div>
<!-- End Flipster List -->
<video id="Player" poster="" preload="none" style="height:1px; max-height:1px; display:none;">
<source type="video/mp4">
Your browser does not support the video tag.
</video>

<script>
function imgError(image) {

image.onerror = "";
image.src = "img/cover-radio/radio_avatar.jpg";
return true;
}
</script>
  <!--Play- botones-->
  <div class="botonesStream" style="display: table; margin: 0 auto;">

     <ul>
     <!--li><a href="javascript:void(0);" class="FlipShare" data-toggle="modal" data-target="#ShareDos"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="60px" height="60px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
<g>
  <path d="M14.084,11.944c-0.592,0-1.13,0.227-1.537,0.597l-4.373-2.23c0.018-0.113,0.029-0.229,0.029-0.348
    c0-0.067-0.004-0.133-0.01-0.198l4.416-2.25c0.399,0.337,0.913,0.541,1.476,0.541c1.264,0,2.287-1.024,2.287-2.287
    c0-1.264-1.023-2.288-2.287-2.288c-1.263,0-2.287,1.024-2.287,2.288c0,0.069,0.004,0.139,0.01,0.207L7.398,8.222
    C7,7.881,6.482,7.675,5.917,7.675c-1.263,0-2.288,1.023-2.288,2.288c0,1.262,1.024,2.287,2.288,2.287
    c0.51,0,0.979-0.168,1.358-0.45l4.527,2.309c-0.001,0.041-0.006,0.081-0.006,0.123c0,1.264,1.023,2.288,2.287,2.288
    s2.287-1.024,2.287-2.288C16.371,12.968,15.348,11.944,14.084,11.944z"/>
</g>
</svg>
</a></li-->
		<li>
    <!--button class="FlipPause" id="ButtonPlay"><i class="fa fa-play"></i></button-->
<?php if(!isset($_GET['fecha_tiempo'])){ ?>
    <button class="PlayerButton playerRadio" id="ButtonPlay"><i class="fa fa-play"></i></button>
<?php }else if($_GET['fecha_tiempo'] < $now){ ?>
    <button class="PlayerButton playerRadio" id="ButtonPlay"><i class="fa fa-play"></i></button>
<? } ?>
<!--
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60px" height="60px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
      <g>
      <polygon points="3.109,15.307 3.109,4.693 16.891,9.999  "/>
      </g>
      </svg>
      </a>
-->		</li>
<li>

<button class="PlayerButton playerRadio" id="ButtonInfo"><span class="infoicon"><i class="fa fa-info-circle"></i></span></button>

</li>
           <!--li><a href="" class="FlipCampana" ><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="60px" height="60px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
<g>
  <path d="M14.719,12.979c-0.279-0.279-0.445-0.652-0.467-1.048l-0.234-4.208c0-1.763-1.514-3.217-3.476-3.446
    c0.013-0.087-0.007-0.19-0.007-0.303c0-0.297-0.239-0.536-0.535-0.536S9.465,3.678,9.465,3.975c0,0.112-0.019,0.216-0.006,0.303
    C7.496,4.507,5.982,5.961,5.982,7.724l-0.233,4.208c-0.021,0.396-0.188,0.768-0.469,1.048l-1.173,1.174v0.534h11.785v-0.534
    L14.719,12.979z"/>
  <path d="M10,16.562c0.74,0,1.339-0.598,1.339-1.338H8.661C8.661,15.964,9.261,16.562,10,16.562"/>
</g>
</svg>
</a></li-->
  </ul>


  </div>


</div>
              <!--El moneto generalizado-->

<?php
  $dia_semana=array('DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB');
  $num_dia=date('w')+7;
  $dia_actual=$dia_semana[$num_dia];

?>
     <div class="dias">
       <ul class="diasContenido"><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-7 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>" href="radio?dia=<?php echo $num_dia-7; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-7]; ?>
            <br />
            <?php echo date('d', strtotime('-7 days')); ?></a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-6 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-6; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-6]; ?><br />
            <?php echo date('d', strtotime('-6 days')); ?></a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-5 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-5; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-5]; ?><br />
            <?php echo date('d', strtotime('-5 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-4 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-4; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-4]; ?><br />
            <?php echo date('d', strtotime('-4 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-3 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-3; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-3]; ?><br />
            <?php echo date('d', strtotime('-3 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-2 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-2; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-2]; ?><br />
            <?php echo date('d', strtotime('-2 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('-1 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia-1; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia-1]; ?><br />
            <?php echo date('d', strtotime('-1 days')); ?>
            </a>
            </li><?php
            $fecha_tiempo=date('Y-m-d');

            ?>
            <li class="diasList activadias">
            <a href="radio?dia=<?php echo $num_dia; ?>">
            <?php echo $dia_semana[$num_dia]; ?>*<br />
            <?php echo date('d'); ?></a>
            </li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('1 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">

            <a class="diasLink" href="radio?dia=<?php echo $num_dia+1; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+1]; ?><br />
            <?php echo date('d', strtotime('1 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('2 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+2; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>"><?php echo $dia_semana[$num_dia+2]; ?><br />
            <?php echo date('d', strtotime('2 days')); ?>
            </a>
            </li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('3 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+3; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+3]; ?><br />
            <?php echo date('d', strtotime('3 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('4 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+4; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+4]; ?><br />
            <?php echo date('d', strtotime('4 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('5 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+5; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+5]; ?><br />
            <?php echo date('d', strtotime('5 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('6 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+6; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+6]; ?><br />
            <?php echo date('d', strtotime('6 days')); ?>
            </a></li><?php
            $fecha_tiempo=date('Y-m-d', strtotime('7 days'));

            ?>
            <li class="diasList <?php if ($fecha_tiempo==$_GET['fecha_tiempo']) { echo "activadias"; } ?>">
            <a class="diasLink" href="radio?dia=<?php echo $num_dia+7; ?>&fecha_tiempo=<?php echo $fecha_tiempo; ?>">
            <?php echo $dia_semana[$num_dia+7]; ?><br />
            <?php echo date('d', strtotime('7 days')); ?>
            </a></li>
       </ul>
   </div>


   <div class="modal fade" id="ShareDos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document">
       <div class="modal-content personalModal">
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
           <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Compartir</h3>

         </div>
         <div class="modal-body">

               <h1 class="text-center text-uppercase" id="ShareTitle"></h1>

               <ul class="activity-share text-uppercase">

                 <li><a href="mailto:email@dominio.com?subject=CONVOY&body=Te invito a que escuches la programación de CONVOY &attachment=https%3A//convoynetwork.com<?php echo $_SERVER['REQUEST_URI']; ?>">
                <svg class="iconosShare"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
   <g>
     <path id="circulo2" class="circuloShare" fill="#B3B3B3" d="M38.537,20c0,10.238-8.299,18.537-18.537,18.537C9.763,38.537,1.463,30.238,1.463,20S9.763,1.463,20,1.463
       C30.238,1.463,38.537,9.763,38.537,20"/>
     <g>
       <polygon fill="#FFFFFF" points="20,23.029 8.371,14.758 8.371,27.443 31.63,27.443 31.63,14.758     "/>
       <polygon fill="#FFFFFF" points="31.512,12.557 8.488,12.557 20,20.72     "/>
     </g>
   </g>
   </svg>

                 </a></li>
                 <li><a href="https://plus.google.com/share?url=https%3A//convoynetwork.com/convoy-webapp/radio" target="_blank">
                   <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
   <g>
     <path fill="#B3B3B3" class="circuloShare" d="M38.828,20c0,10.398-8.429,18.828-18.828,18.828C9.602,38.828,1.172,30.398,1.172,20S9.602,1.172,20,1.172
       C30.399,1.172,38.828,9.602,38.828,20"/>
     <g>
       <polygon fill="#FFFFFF" points="32.209,13.271 28.559,13.271 28.559,9.012 27.343,9.012 27.343,9.086 27.343,13.271
         23.084,13.271 23.084,15.095 27.343,15.095 27.343,18.745 28.559,18.745 28.559,15.095 32.209,15.095     "/>
       <path fill="#FFFFFF" d="M18.438,21.606c-0.595-0.421-1.731-1.445-1.731-2.046c0-0.705,0.201-1.053,1.263-1.881
         c1.087-0.85,1.856-1.849,1.856-3.239c0-1.654-0.736-3.603-2.119-3.603h2.085l1.471-1.825h-6.575c-2.948,0-5.723,2.419-5.723,5.006
         c0,2.644,2.01,4.871,5.009,4.871c0.208,0,0.411,0.042,0.609,0.028c-0.194,0.373-0.333,0.816-0.333,1.251
         c0,0.735,0.396,1.342,0.895,1.828c-0.378,0-0.742,0.017-1.14,0.017c-3.651,0-6.462,2.328-6.462,4.74
         c0,2.375,3.081,3.862,6.732,3.862c4.163,0,6.463-2.361,6.463-4.736C20.738,23.974,20.176,22.834,18.438,21.606 M14.926,18.311
         c-1.695-0.05-3.306-1.894-3.597-4.119c-0.293-2.226,0.843-3.928,2.537-3.877c1.693,0.051,3.304,1.835,3.597,4.06
         C17.756,16.6,16.618,18.362,14.926,18.311 M14.262,29.566c-2.523,0-4.347-1.598-4.347-3.517c0-1.881,2.261-3.447,4.785-3.42
         c0.589,0.007,1.138,0.102,1.636,0.263c1.371,0.952,2.353,1.491,2.63,2.577c0.053,0.221,0.08,0.446,0.08,0.678
         C19.046,28.066,17.811,29.566,14.262,29.566"/>
     </g>
   </g>
   </svg>
                 </a></li>
                 <li class=""><a href="https://twitter.com/home?status=https%3A//convoynetwork.com/convoy-webapp/radio" target="_blank">
                   <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
   <g>
     <path fill="#B3B3B3" class="circuloShare" d="M38.453,20c0,10.191-8.262,18.452-18.453,18.452C9.809,38.452,1.547,30.191,1.547,20
       C1.547,9.809,9.809,1.548,20,1.548C30.191,1.548,38.453,9.809,38.453,20"/>
     <path fill="#FFFFFF" d="M32.605,12.668c-0.891,0.396-1.848,0.663-2.854,0.783c1.025-0.615,1.814-1.589,2.186-2.75
       c-0.961,0.569-2.023,0.982-3.156,1.206c-0.906-0.966-2.197-1.569-3.625-1.569c-2.746,0-4.969,2.225-4.969,4.968
       c0,0.39,0.043,0.769,0.128,1.133c-4.129-0.207-7.791-2.186-10.241-5.192c-0.427,0.735-0.673,1.588-0.673,2.499
       c0,1.724,0.877,3.244,2.21,4.135c-0.814-0.025-1.581-0.249-2.25-0.621C9.36,17.281,9.36,17.303,9.36,17.324
       c0,2.407,1.713,4.415,3.986,4.872c-0.417,0.113-0.855,0.175-1.309,0.175c-0.32,0-0.631-0.032-0.935-0.09
       c0.632,1.974,2.467,3.41,4.641,3.451c-1.7,1.333-3.842,2.127-6.17,2.127c-0.401,0-0.796-0.023-1.186-0.07
       c2.2,1.41,4.811,2.233,7.617,2.233c9.14,0,14.136-7.571,14.136-14.138c0-0.215-0.004-0.43-0.014-0.642
       C31.098,14.54,31.941,13.665,32.605,12.668"/>
   </g>
   </svg>
                 </a></li>
                 <li class=""><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//convoynetwork.com/" target="_blank">
                   <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
   <g>
     <path fill="#B3B3B3" class="circuloShare" d="M38.953,20c0,10.467-8.485,18.953-18.953,18.953S1.047,30.467,1.047,20S9.532,1.047,20,1.047
       S38.953,9.533,38.953,20"/>
     <path fill="#FFFFFF" d="M9.3,16.643c0,0-1.111-0.31-1.111,1.111l0.662,9.188c0,0,0.111,0.9,0.844,0.9h6.612
       c0,0,0.761,0.084,0.761-1.379c0,0,0.393,0.646,1.913,0.646h8.554c0,0,2.278-0.028,2.278-2.278c0,0,0.015-0.746-0.309-1.07
       c0,0,0.957-0.506,0.957-1.435c0,0,0.14-0.618-0.282-1.042c0,0,1.407-0.843,0.704-2.447c0,0,0.929-0.45,0.929-1.829
       c0,0,0-1.802-1.802-1.802h-5.909c0,0,0.788-2.701,0.281-4.333c0,0-0.899-3.292-2.897-3.292c0,0-2.054,0.225-1.379,2.307
       c0,0,0.675,2.026-0.731,3.798l-2.307,3.406c0,0,0.112-0.451-0.677-0.451H9.3z"/>
   </g>
   </svg>

                 </a></li>

               </ul>
               <input  class="text-uppercase form-control" type="button" placeholder="Aceptar"  data-dismiss="modal" aria-label="Close" style="cursor: pointer">

         </div>

       </div>
     </div>
   </div>