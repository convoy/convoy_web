<?php
if(!isset($_SESSION)){
	session_start();
	require_once('../php-libs/function.SessionValidate.php');
	}
if(!isset($DB)){
	require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
	}

$FollowerID = $_POST['ContactoID'];
?> 
   <section class="MensajeContenido" style="float: left; height: 430px;">
    <header class="MensajeCabecera">
         <h4 class="MensajeCabeceraTitulo">USUARIO</h4>
         <p class="MensajeCabeceraSubtitulo"> </p>
    </header>


		<!--BLOQUE DE MENSAJES-->
    <section class="MensajeCtn" id="mensaje_res">
<?php
$FromDate = '';
if(isset($_POST['FromDate'])){
	$FromDate = $_POST['FromDate'];
	}
$LastMessage = '';
if(isset($_POST['LasMessageID'])){
	$LastMessage = $_POST['LasMessageID'];
	}


	$sT = "
	SELECT COUNT(*) AS Total FROM Messages WHERE (mess_FromUserID='".$_SESSION['SessionUserID']."' AND mess_ToUserID='".$FollowerID."') OR (mess_FromUserID='".$FollowerID."' AND mess_ToUserID='".$_SESSION['SessionUserID']."')";
	$qT = $DB->query($sT);
	$rT = $qT->fetch_array();
	$tT = $rT['Total'];
	$FromMessage = '';
	$Condition='';

	if($FromDate==''){
		if($tT<10){
			$Limit=0;
			}
		else{
			$Limit = ($tT-10);
			}
		}
	else{
		$Condition=" AND mess_Datetime<='".$FromDate."' AND mess_messageID<'".$LastMessage."' ";
		$Limit = '';
		}
	$s = "
	SELECT
		mess_messageID, 
		mess_FromUserID,
		mess_ToUserID,
		S.user_Login AS FromUser, 
		R.user_Login AS ToUser, 
		S.user_FullName AS FromFullname, 
		R.user_FullName AS ToFullname, 
		S.user_Avatar AS FromURLImage, 
		R.user_Avatar AS ToURLImage, 
		mess_Message,
		mess_Datetime,
		Messages.mess_IPAddress AS UserIP, 
		Messages.mess_Status AS ReadStatus 
	FROM Messages 
	INNER JOIN Users AS S ON S.user_Userid=mess_FromUserID
	INNER JOIN Users AS R ON R.user_Userid=mess_ToUserID
	WHERE
		(mess_FromUserID='".$_SESSION['SessionUserID']."' AND mess_ToUserID='".$FollowerID."')
	OR
		(mess_FromUserID='".$FollowerID."' AND mess_ToUserID='".$_SESSION['SessionUserID']."')
		".$Condition."
	ORDER BY mess_Datetime ASC
	LIMIT ".$Limit.", 10;";
	$q = $DB->query($s);
	$t = $q->num_rows;
	$mensaje['Messages'] = array(); 
	if($t>0){
		while($r = $q->fetch_array()){
			$row['MessageID'] = $r['mess_messageID'];
			if($_SESSION['SessionUserID']==$r['mess_FromUserID']){
				$row['MeID'] = $r['mess_FromUserID'];
				$row['Me'] = $r['FromUser'];
				$row['MeFullname'] = $r['FromFullname'];
				$row['MeURLImage'] = $r['FromURLImage'];
				$row['FriendID'] = $r['mess_ToUserID'];
				$row['Friend'] = $r['ToUser'];
				$row['FriendFullname'] = $r['ToFullname'];
				$row['FriendURLImage'] = $r['ToURLImage'];
				$row['SentMessage'] = utf8_encode($r['mess_Message']);
				$row['ReceivedMessage'] = "";
				echo '
        <article class="MensajeOut">
        	<article class="MensajeOutBloque">
          	<article class="MensajeOutContenido">
            	<p>'.$r['mess_Message'].'</p>
						</article>
						<article class="MensajeFecha">
            	<p>'.$r['mess_Datetime'].'</p>
						</article>
					</article>
				</article>			
				';
				}
			else if($_SESSION['SessionUserID']==$r['mess_ToUserID']){
				$row['MeID'] = $r['mess_ToUserID'];
				$row['Me'] = $r['ToUser'];
				$row['MeFullname'] = $r['ToFullname'];
				$row['MeURLImage'] = $r['ToURLImage'];
				$row['FriendID'] = $r['mess_FromUserID'];
				$row['Friend'] = $r['FromUser'];
				$row['FriendFullname'] = $r['FromFullname'];
				$row['FriendURLImage'] = $r['FromURLImage'];
				$row['SentMessage'] = "";
				$row['ReceivedMessage'] = utf8_encode($r['mess_Message']);
				echo '
					<article class="MensajeIn">
						<article class="MensajeOutBloque">
							<article class="MensajeOutContenido">
								<p>'.$r['mess_Message'].'</p>
							</article>
							<article class="MensajeFecha">
								<p>'.$r['mess_Datetime'].'</p>
							</article>
						</article>
					</article>
				';
				}
			$row['FullDate'] = $r['mess_Datetime'];
			}
		}
	else{
		echo 'No hay mensajes';
		}
?>
				<!--MAQUETACIÓN DE MENSAJE DE ENTRADA-->
				<!--
        <article class="MensajeIn">
        	<article class="MensajeOutBloque">
          	<article class="MensajeOutContenido">
            	<p>algunos datos</p>
						</article>
						<article class="MensajeFecha">
            	<p>Time</p>
						</article>
					</article>
				</article>
				-->
        <!--MAQUETACIÓN DE MENSAJE DE ENTRADA-->
        
        
				<!--MAQUETACIÓN DE MENSAJE DE SALIDA-->
        <!--
        <article class="MensajeOut">
        	<article class="MensajeOutBloque">
          	<article class="MensajeOutContenido">
            	<p>algunos datos</p>
						</article>
						<article class="MensajeFecha">
            	<p>Time</p>
						</article>
					</article>
				</article>
				-->
        <!--MAQUETACIÓN DE MENSAJE DE SALIDA-->

		<!--BLOQUE DE MENSAJES-->
		</section>

		<!-- FORMULARIO DE CHAT -->
    <section class="MensajeEnvio" >
       
       <form action="proc/guardar_mensaje.php" id="mensaje_ctrl" method="post">
           <input type="hidden" name="id_mensaje_padre" value="" />

           <input type="hidden" name="id_usuario_origen" value="" />
           <input type="hidden" name="id_usuario_destino" value="" />
           <input type="text" class="inputEnviar form-control form-per" placeholder="Enviar Mensaje" name="mensaje">
           <button class="btnEnviar" type="submit">Enviar</button>
       </form>
        
        
    </section>
    
    
</section>
 
