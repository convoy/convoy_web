<div class="news-title text-uppercase" style="display: table; margin: 0 auto;width: 92%">
	<form class="EditarPerfilForm" action="php-scripts/problem-report.php" method="post" enctype="multipart/form-data">
  <h1 style="font-size: 25px;">REPORTES DE PROBLEMAS</h1>
  <p class="font-pers" style="font-size: 1em;">Reporte cualquier incidencia con respecto al sitio o a tu cuenta.</p>

  <p class="font-pers" style="font-size: 1em;">
    <select style="width: 100% !important;" class="form-control" name="probType" id="probType">
        <option value="1">Algo no funciona</option>
        <option value="2">Retroalimentación General</option>
        <option value="3">Problemas de suscripción</option>
    </select>
  </p>

  <p class="font-pers" style="font-size: 1em;">
  <textarea class="form-control" name="reporte" placeholder="Escribe tu reporte"></textarea>
  </p>
  
  <p class="font-pers" style="font-size: 1em;">
  <button class="btn EditarPerfilBtn" type="submit">Enviar</button>
  </p>
  
  </form>
  </div>
