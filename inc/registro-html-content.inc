<?php

require_once(__DIR__ . '/../php-libs/function.ConnDB.php');


/////////////////////////////////////////////////////////////////////////////////////////////////////

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
if (PHP_VERSION < 6) {
$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
}

$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

switch ($theType) {
case "text":
$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
break;
case "long":
case "int":
$theValue = ($theValue != "") ? intval($theValue) : "NULL";
break;
case "double":
$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
break;
case "date":
$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
break;
case "defined":
$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
break;
}
return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "RegisterForm")) {

    $Login = $_POST['Login'];
    $Password = $_POST['Psswd'];
    $Email = $_POST['Email'];
    $AuthCode = rand(100000,999999);
    $Birthdate = $_POST['BD_Yrs']."-".$_POST['BD_Mon']."-".$_POST['BD_Day'];

    $sC="SELECT user_Login FROM Users WHERE user_Login='".$Login."'";
		$qC = $DB->query($sC);
		$tC = $qC->num_rows;
		$rC = $qC->fetch_array();
    $sD="SELECT user_Email FROM Users WHERE user_Email='".$Email."'";
		$qD = $DB->query($sD);
		$tD = $qD->num_rows;
		$rD = $qD->fetch_array();

    if($tC==0){
        $s = "INSERT INTO Users(user_Login, user_Password, user_Email, user_Birthdate, user_AuthCode)".
        " VALUES ('".$Login."', '".$Password."', '".$Email."', CAST('". $Birthdate ."' AS DATE), '".$AuthCode."')";
        $q = $DB->query($s);
        $a = $DB->affected_rows;
        $id = $DB->insert_id;

    }

    $DB->close();
}
?>

<div <?php if($page == 'registro'){echo'class="registroStyle text-uppercase" style="width: 40%;margin:0 auto;"';}else{echo"style='width: 95%;margin:0 auto;'";}?>>
     <img class="logo" src="img/logo-convoy.png" alt="" />
     <h1 class="text-center">Registro</h1>
     <form
        onsubmit="return validate(this);"
        name="RegisterForm"
        id="RegisterForm"
        method="post"
        action="registro"
        enctype="multipart/form-data"
        id="reaccion">
        <input type="hidden" name="MM_insert" value="RegisterForm">
       <div class="form-group col-md-offset-3 col-md-6">
         <input type="text" class="form-control form-per" name="Login" id="Login" placeholder="Nombre de usuario" required autocomplete="off">
       </div>
       <div class="form-group col-md-offset-3 col-md-6">
         <input type="password" class="form-control form-per" name="Psswd" id="Psswd" placeholder="contraseña" required>
       </div>
       <div class="form-group col-md-offset-3 col-md-6">
         <input type="password" class="form-control form-per" name="Psswd_Retype" id="Psswd_Retype" placeholder=" Repetir contraseña" required>
       </div>
       <div class="form-group col-md-offset-3 col-md-6">
         <input type="email" class="form-control form-per" name="Email" id="Email" placeholder="Correo electrónico" required autocomplete="off">
       </div>
       <div class="form-group col-md-offset-3 col-md-6">
         <input type="email2" class="form-control form-per" name="Email_Retype" id="Email_Retype" placeholder="Confirmar correo electrónico" required autocomplete="off">
       </div>
       <div class="form-group col-md-offset-2 col-md-8 form-inline">
        <h2 class="text-center">Fecha de nacimiento</h2>
         <select class="form-control text-uppercase" name="BD_Day" id="BD_Day" required>
           <option value="">Día</option>
           <option>01</option>
           <option>02</option>
           <option>03</option>
           <option>04</option>
           <option>05</option>
           <option>06</option>
           <option>07</option>
           <option>08</option>
           <option>09</option>
           <option>10</option>
           <option>11</option>
           <option>12</option>
           <option>13</option>
           <option>14</option>
           <option>15</option>
           <option>16</option>
           <option>17</option>
           <option>18</option>
           <option>19</option>
           <option>20</option>
           <option>21</option>
           <option>22</option>
           <option>23</option>
           <option>24</option>
           <option>25</option>
           <option>26</option>
           <option>27</option>
           <option>28</option>
           <option>29</option>
           <option>30</option>
           <option>31</option>
         </select>
         <select class="form-control text-uppercase" name="BD_Mon" id="BD_Mon" required>
           <option value="">Mes</option>
           <option value="01">Enero</option>
           <option value="02">Febrero</option>
           <option value="03">Marzo</option>
           <option value="04">Abril</option>
           <option value="05">Mayo</option>
           <option value="06">Junio</option>
           <option value="07">Julio</option>
           <option value="08">Agosto</option>
           <option value="09">Septiembre</option>
           <option value="10">Octubre</option>
           <option value="11">Noviembre</option>
           <option value="12">Diciembre</option>
         </select>
         <select class="form-control text-uppercase" name="BD_Yrs" id="BD_Yrs" required>
           <option value="">Año</option><?php
           for ($i=105; $i>0; $i--) { ?><option><?php echo $i+1910; ?></option><?php } ?>

         </select>
       </div>

       <div class="form-group col-md-12 text-center regText clearfix">
          Al presionar "Registrar" <br>Estás Conforme <br>
            con nuestros <a href="https://convoynetwork.com/terminos-y-condiciones/" target="_blank" title="Terminos">Términos y condiciones</a>
          y la <a href="https://convoynetwork.com/privacidad/" title="Politica" target="_blank">Política de privacidad</a>
       </div>
       <div class="form-group col-md-12">
         <!--button type="submit" class="btn btn-convoy-enter f" name="Btn_Register" name="Btn_Register">Registrar</button-->
        <br>
<input type="submit" class="btn btn-block" style="max-width:250px;" name="Btn_Register" name="Btn_Register" value="Registrar">
        <br>
       <div class="text-center text-uppercase whiteA">¿Ya Tienes una cuenta? <a href="#" data-dismiss="modal" aria-label="Close" class="ColorWhite" title="Iniciar sesión">Iniciar sesión</a> </div>
       </div>
       <div id="content"></div>

     </form>
        <div id="RegisterResponse" styl></div>
<script type="text/javascript" src="js/register-validate.js"></script>
<?php
if((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "RegisterForm")){
    
    if($a>0){ ?>
        <script>
        $('#RegisterForm').fadeOut();
        var Msg = "Te has registrado exitosamente! <br> <h3><a href='login'>Inicia tu sesión AQUÍ</a></h3>";
        $('#RegisterResponse').html(Msg);
        </script>
        <?php
        }else{ ?>
        <script>
        var Msg = "No ha sido posible registrarte. inténtalo nuevamente.";
        $('#RegisterResponse').html(Msg);
        </script>
        <?php
    }
    if($tC > 0){ ?>
        <script>
        var Msg = "Ese usuario ya existe. Intenta nuevamente.";
        $('#RegisterResponse').html(Msg);
        </script>
        <?php
    }
    if($tD > 0){ ?>
        <script>
        var Msg = "Ese correo ya está registrado. Intenta nuevamente.";
        $('#RegisterResponse').html(Msg);
        </script>
        <?php
    }

}
?>
 </div>
