<script type="text/javascript">
  function cargar_podcast(id_podcast) {
    console.log(id_podcast);
    jQuery('#carga_podcast_new').load('./proc/podcasts-play.php?id_podcast=' + id_podcast);
  }
  function cargar_track(id_track){
    jQuery('#media_player').load('./proc/controls.php?id_track=' + id_track)
  }
</script>
<!--Navegación-->
<?php 
require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
require_once('php-libs/function.General.php');
require_once('nav.inc');
require_once('tabs-welcome.inc');
?>


  <div class="container-fluid ESuper">
  <div class="row">
  <div class="col-md-12">
  <div style="width: 100%; height: 100px; float: left"></div>
  <div id="carga_podcast_new">
<?php
$s = "
SELECT 
podc_PodcastID,
podc_Title,
podc_Description,
podc_Image,
podc_Creation,
podc_Status

FROM
Podcast
ORDER BY 
podc_Order
ASC
";
$q = $DB->query($s);
while($r = $q->fetch_array()){
	echo '
		<div class="podcasts" id="podcasts"><!--inicia el podcasts-->
			<a href="podcasts-seasons?P='.$r['podc_PodcastID'].'"  class="play-c"  title="'.$r['podc_Title'].'">
			<div class="podcastsImg"><!--imagen-->
				<img src="img/cover-podcast/'.$r['podc_PodcastID'].'/'.$r['podc_Image'].'" alt="muestra" class="img-responsive" />
			</div><!--termina portada-->
			<div class="podcastsTexto">
				<h3 class="text-uppercase podcastsTextoTitulo">'.$r['podc_Title'].'</h3>
				<p class="podcastsTextoContenido">'.$r['podc_Description'].'</p>
			</div></a>
		</div><!--Termina el podcasts-->
	';
	}

$DB->close();
?>
</div>
   </div>
  </div>

</div>
<div>
