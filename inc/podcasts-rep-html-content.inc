<!--Navegación-->
<?php include 'nav.inc';?>
<!--nav secundaria-->
<div class="menu-secundario">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-lx-12">
        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
      <ul id="myTabs" class="nav nav-tabs" role="tablist">
        <li role="presentation" class="dropdown active"><a href="#home" class="text-uppercase" id="home-tab" role="tab" data-toggle="tab" aria-controls="home"> <i class="glyphicon glyphicon-list-alt"></i> <span class="desaparece">Notificaciones</span></a></li>
        <li role="presentation" class=""><a href="#profile" role="tab" class="text-uppercase" id="profile-tab" data-toggle="tab" aria-controls="profile"> <i class="fa fa-rocket"></i> <span class="desaparece">Podcasts</span></a></li>
        <li role="presentation" class=""><a href="#myTabDrop" id="myTabDrop1" class="text-uppercase" data-toggle="tab" aria-controls="myTabDrop1-contents"><i class="fa fa-microphone"></i> <span class="desaparece"> Radio</span></a></li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home" aria-labelledby="home-tab">
          <?php include 'notificactions-html-content.inc';?>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile" aria-labelledby="profile-tab" aria-expanded="true">
          <?php include 'podcasts-play-html-content.inc';?>
        </div>

        <div role="tabpanel" class="tab-pane" id="myTabDrop" aria-labelledby="myTabDrop1">
                <?php include 'radio-html-content.inc';?>
        </div>

      </div>
    </div>
      </div>

    </div>

  </div>
</div>

<!--termna nav secundaria-->
<!--Comienza contenido-->
