<?php 
header('Content-Type: text/html; charset=utf-8');
require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
require_once('php-libs/function.General.php');
require_once('nav.inc');
require_once('tabs-welcome.inc');
$UserID = $_SESSION['SessionUserID'];
?>
<!--nav secundaria-->
<div id="ver_perfil"></div>
<div class="container"><!--base bootstrap-->
  <div class="row"><!--base bootstrap dos-->
   <div class="col-md-12 col-sm-12 col-lx-12 innerNews">
<?php
$NewsID = $_GET['NewsID'];
$s="
SELECT 
	news_NewsID,
	news_Title,
	news_Brief,
	news_Content,
	news_Image,
	DATE_FORMAT(news_Datetime,'%Y-%m-%e') AS news_Date,
    (SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID AND like_UserID='".$UserID."') AS UserLike,
	news_Tags
FROM News 
WHERE news_NewsID='".$NewsID."'";
$q=$DB->query($s);
$r=$q->fetch_array();
?>

    <div class="col-md-12 col-sm-12 col-lx-12 ESuper">
    <!--Columna inicia-->
       <!---->
         <div class="text-contener col-md-12 col-sm-12 innerNews">
           <p><?php echo DateFront($r['news_Date']);?></p>
           <h1 class="text-uppercase news-title"><?php echo $r['news_Title']; ?></h1>
           <img src="img/img-noticias/<?php echo $r['news_Image']; ?>" alt="<?php echo $r['news_Title']; ?>" class="img-responsive" style="width: 100%; height: auto" />
           <p class="font-pers"><?php echo $r['news_Brief']; ?></p>

           <p class="font-pers" title="newsContent"><?php echo $r['news_Content']; ?></p>
         </div>

            <div class="col-md-4 col-sm-12 ctn-share">
                 <ul class="share"> 
                    <li><a onClick="likeFunction('<?php echo base64_encode($UserID)?>', '<?php echo base64_encode('N'); ?>', <?php echo $r['news_NewsID']; ?>);" title=" Like"><i id="arDe<?php echo $r['news_NewsID']; ?>" class="icons iconsLike<?php if($r['UserLike'] == 1){ echo ' podcLikeRelleno'; } ?>"></i><span class="hid"> Like</span></a></li>
                    <!--li><a href="javascript:void(0);" title="Share" data-toggle="modal" data-target="#share<?php echo $r['news_NewsID']; ?>"><i class="icons iconsShare"></i><span class="hid"> Share</span></a></li-->
                 </ul>
             </div>
    </div><!--Termina columna-->
  <script type="text/javascript" src="js/like.js"></script>
<script type="text/javascript" src="js/app-tabs.js"></script>
    <div class="modal fade" id="myModal<?php echo $r['news_NewsID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content personalModal">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Guardar noticia</h3>

      </div>
      <div class="modal-body">

            <h1 class="text-center text-uppercase"><?php echo $r['news_Title']; ?></h1>
            <div id="guardar_elemento_tal_<?php echo $r['news_NewsID']; ?>">
            <h2 class="text-center text-uppercase">Escoger carpeta</h2>
            <ul class="activity-osb text-uppercase">
						<?php
						$sC ="
						SELECT
						
						FROM 
						Collections 
						WHERE coll_UserID='".$_SESSION['SessionUserID']."'";
            $qC = $base_datos->query($sC);
            while($rC=$qC->fetch_array()){
							echo '<li><a class="btn btnSave" href="javascript:guardar_elemento(\'noticia\', \''.$r['news_NewsID'].'\, \''.$rC['id_carpeta'].'\');">'.$rC['nombre_carpeta'].'</a></li>';
               } 
							 ?>
            </ul>
            </div>

      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>

<div class="modal fade" id="share<?php echo $r['id_noticia']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content personalModal">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h3 class="modal-title text-center text-uppercase" id="myModalLabel">Compartir</h3>

      </div>
      <div class="modal-body">

            <h1 class="text-center text-uppercase"><?php echo $r['cabeza']; ?></h1>

            <ul class="activity-share text-uppercase">
              <li><a href="#">

                  <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" id="circuloShare" d="M37.828,20c0,9.845-7.981,17.828-17.828,17.828c-9.846,0-17.828-7.982-17.828-17.828
    c0-9.846,7.982-17.828,17.828-17.828C29.847,2.172,37.828,10.154,37.828,20"/>
  <path fill="#FFFFFF" d="M33.292,17.174c-1.104-1.505-24.813-2.046-26.977,0c0,0-1.324,2.965,0,5.804c0,0,1.275,0.297,3.563,0.598
    l3.581,7.25v-6.875c1.867,0.148,4.048,0.256,6.506,0.256c8.392,0,13.326-1.229,13.326-1.229
    C34.617,20.139,33.292,17.174,33.292,17.174z"/>
</g>
</svg>

              </a></li>
              <li><a href="#">
                <svg class="iconosShare"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path id="circulo2" class="circuloShare" fill="#B3B3B3" d="M38.537,20c0,10.238-8.299,18.537-18.537,18.537C9.763,38.537,1.463,30.238,1.463,20S9.763,1.463,20,1.463
    C30.238,1.463,38.537,9.763,38.537,20"/>
  <g>
    <polygon fill="#FFFFFF" points="20,23.029 8.371,14.758 8.371,27.443 31.63,27.443 31.63,14.758     "/>
    <polygon fill="#FFFFFF" points="31.512,12.557 8.488,12.557 20,20.72     "/>
  </g>
</g>
</svg>

              </a></li>
              <li><a href="https://plus.google.com/share?url=http%3A//convoynetwork.com<?php echo $_SERVER["REQUEST_URI"]; ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.828,20c0,10.398-8.429,18.828-18.828,18.828C9.602,38.828,1.172,30.398,1.172,20S9.602,1.172,20,1.172
    C30.399,1.172,38.828,9.602,38.828,20"/>
  <g>
    <polygon fill="#FFFFFF" points="32.209,13.271 28.559,13.271 28.559,9.012 27.343,9.012 27.343,9.086 27.343,13.271
      23.084,13.271 23.084,15.095 27.343,15.095 27.343,18.745 28.559,18.745 28.559,15.095 32.209,15.095     "/>
    <path fill="#FFFFFF" d="M18.438,21.606c-0.595-0.421-1.731-1.445-1.731-2.046c0-0.705,0.201-1.053,1.263-1.881
      c1.087-0.85,1.856-1.849,1.856-3.239c0-1.654-0.736-3.603-2.119-3.603h2.085l1.471-1.825h-6.575c-2.948,0-5.723,2.419-5.723,5.006
      c0,2.644,2.01,4.871,5.009,4.871c0.208,0,0.411,0.042,0.609,0.028c-0.194,0.373-0.333,0.816-0.333,1.251
      c0,0.735,0.396,1.342,0.895,1.828c-0.378,0-0.742,0.017-1.14,0.017c-3.651,0-6.462,2.328-6.462,4.74
      c0,2.375,3.081,3.862,6.732,3.862c4.163,0,6.463-2.361,6.463-4.736C20.738,23.974,20.176,22.834,18.438,21.606 M14.926,18.311
      c-1.695-0.05-3.306-1.894-3.597-4.119c-0.293-2.226,0.843-3.928,2.537-3.877c1.693,0.051,3.304,1.835,3.597,4.06
      C17.756,16.6,16.618,18.362,14.926,18.311 M14.262,29.566c-2.523,0-4.347-1.598-4.347-3.517c0-1.881,2.261-3.447,4.785-3.42
      c0.589,0.007,1.138,0.102,1.636,0.263c1.371,0.952,2.353,1.491,2.63,2.577c0.053,0.221,0.08,0.446,0.08,0.678
      C19.046,28.066,17.811,29.566,14.262,29.566"/>
  </g>
</g>
</svg>
              </a></li>
              <li class=""><a href="https://twitter.com/home?status=http%3A//convoyinteractive.com<?php echo $_SERVER["REQUEST_URI"]; ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.453,20c0,10.191-8.262,18.452-18.453,18.452C9.809,38.452,1.547,30.191,1.547,20
    C1.547,9.809,9.809,1.548,20,1.548C30.191,1.548,38.453,9.809,38.453,20"/>
  <path fill="#FFFFFF" d="M32.605,12.668c-0.891,0.396-1.848,0.663-2.854,0.783c1.025-0.615,1.814-1.589,2.186-2.75
    c-0.961,0.569-2.023,0.982-3.156,1.206c-0.906-0.966-2.197-1.569-3.625-1.569c-2.746,0-4.969,2.225-4.969,4.968
    c0,0.39,0.043,0.769,0.128,1.133c-4.129-0.207-7.791-2.186-10.241-5.192c-0.427,0.735-0.673,1.588-0.673,2.499
    c0,1.724,0.877,3.244,2.21,4.135c-0.814-0.025-1.581-0.249-2.25-0.621C9.36,17.281,9.36,17.303,9.36,17.324
    c0,2.407,1.713,4.415,3.986,4.872c-0.417,0.113-0.855,0.175-1.309,0.175c-0.32,0-0.631-0.032-0.935-0.09
    c0.632,1.974,2.467,3.41,4.641,3.451c-1.7,1.333-3.842,2.127-6.17,2.127c-0.401,0-0.796-0.023-1.186-0.07
    c2.2,1.41,4.811,2.233,7.617,2.233c9.14,0,14.136-7.571,14.136-14.138c0-0.215-0.004-0.43-0.014-0.642
    C31.098,14.54,31.941,13.665,32.605,12.668"/>
</g>
</svg>
              </a></li>
              <li class=""><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//convoyinteractive.com/<?php echo $_SERVER["REQUEST_URI"]; ?>" target="_blank">
                <svg class="iconosShare" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<g>
  <path fill="#B3B3B3" class="circuloShare" d="M38.953,20c0,10.467-8.485,18.953-18.953,18.953S1.047,30.467,1.047,20S9.532,1.047,20,1.047
    S38.953,9.533,38.953,20"/>
  <path fill="#FFFFFF" d="M9.3,16.643c0,0-1.111-0.31-1.111,1.111l0.662,9.188c0,0,0.111,0.9,0.844,0.9h6.612
    c0,0,0.761,0.084,0.761-1.379c0,0,0.393,0.646,1.913,0.646h8.554c0,0,2.278-0.028,2.278-2.278c0,0,0.015-0.746-0.309-1.07
    c0,0,0.957-0.506,0.957-1.435c0,0,0.14-0.618-0.282-1.042c0,0,1.407-0.843,0.704-2.447c0,0,0.929-0.45,0.929-1.829
    c0,0,0-1.802-1.802-1.802h-5.909c0,0,0.788-2.701,0.281-4.333c0,0-0.899-3.292-2.897-3.292c0,0-2.054,0.225-1.379,2.307
    c0,0,0.675,2.026-0.731,3.798l-2.307,3.406c0,0,0.112-0.451-0.677-0.451H9.3z"/>
</g>
</svg>

              </a></li>

            </ul>
            <input  class="text-uppercase form-control" type="button" placeholder="Aceptar"  data-dismiss="modal" aria-label="Close" style="cursor: pointer">

      </div>
      
    </div>
  </div>
</div>




  </div></div></div>


 

