<!--Navegación-->
<?php include 'nav.inc';?>
<?php include 'tabs-welcome.inc';?>
<!--nav secundaria-->

<div class="container ESuper"><!--base bootstrap-->
  <div class="row"><!--base bootstrap dos-->
   <div class="col-md-12 col-sm-12 col-lx-12 ESuper"><!--Columna inicia-->
       <!---->
         <div class="text-contener col-md-11 col-sm-12">

<h3 class="tituloBase">Aviso de privacidad </h3>
<p class="textoBase"> <p><strong>CONVOY ENTERTAINMENT S.A. DE C.V.</strong></p>
<p class="p1"><span class="s1">Bienvenido a nuestros Términos y Condiciones de uso. Los Términos y Condiciones de uso son importantes y afectan tus derechos legales, por lo que te recomendamos que los leas detenidamente, al igual que nuestra </span><span class="s1">Política de privacidad</span><span class="s2">.</span></p>
<p class="p4"><span class="s1">Si decides aceptar los siguientes Términos y Condiciones de uso (en lo sucesivo los “T&amp;C”), tu aceptación constituye una acción legalmente vinculante con Convoy Entertainment, S.A. de C.V., sociedad debidamente constituida en México, con domicilio social en Acapulco Número 62 interior 1, Colonia Roma Norte, Delegación Cuauhtémoc, C.P. 06700 en México D.F., con RFC- CEN150727B57 (en lo sucesivo “CONVOY”). A través de éstos T&amp;C se regirá el pago a la </span><span class="s3">suscripción móvil y web de audio <i>on-demand</i> en español la cual presenta contenido original, hablado y musical, con entrevistas, especiales y el catálogo completo de <i>El podcast de Olallo Rubio </i>en el sitio web www.convoynetwork.com<i>. </i>En nuestro sitio encontrarás un <i>stream</i> sin anuncios y <i>on-demand </i>(en lo sucesivo “CONVOY APP”). CONVOY APP, disponible para web e iOS, ofrece la experiencia más conveniente y personalizable para escuchar podcasts y mixtapes premium por una cuota mensual de bajo costo a través del </span><span class="s1">sitio web <a href="http://www.convoynetwork.comx"><span class="s4">www.convoynetwork.com</span></a> (en adelante, el “Servicio CONVOY”). Al pagar la suscripción del Servicio te obligas por los T&amp;C a continuación referidos.</span></p>
<p class="p1"><span class="s1">Recuerda que para usar el Servicio CONVOY y acceder a tu Contenido, debes tener (i) 18 años o más, o 13 años o más y tener el consentimiento de los padres respecto de los Contratos, (ii) tener capacidad para celebrar un contrato vinculante con nosotros y que ninguna ley vigente le impida hacerlo, y (iii) ser residente de un país que proporcione el Servicio CONVOY. Además, deberás garantizar que toda la información de inscripción que le proporciones a CONVOY es veraz, fiel y completa, y acepta que será así en todo momento.</span></p>
<p class="p4"><span class="s1"><b>Derechos de los Usuarios de CONVOY APP.</b></span></p>
<p class="p4"><span class="s1">Una vez que realices el pago por la suscripción del Servicio CONVOY, tendrás el carácter de “USUARIO” de nuestra CONVOY APP lo que te autorizará a acceder a todos nuestros contenidos. Para acceder a los contenidos y al Servicio CONVOY deberás abrir una cuenta en nuestra CONVOY App, proporcionarnos la información requerida para poder brindarte el Servicio CONVOY y aceptar los T&amp;C y nuestra Política de Privacidad. Si no aceptas ambas, no podrás ser usuario de CONVOY APP.</span></p>
<p class="p4"><span class="s1">CONVOY podrá, en todo momento y por así convenir a sus intereses, modificar los presentes T&amp;C, no obstante lo anterior, toda modificación sustancial será comunicada a través de nuestro sitio web o a través de CONVOY APP. Una vez notificados de los cambios, si decides continuar utilizando nuestro Servicio y/o la CONVOY APP, ello constituirá tu plena aceptación sobre dichas modificaciones.</span></p>
<p class="p4"><span class="s1"><b>Servicio de </b></span><span class="s2"><b>Prueba.</b></span></p>
<p class="p1"><span class="s1">Como parte de nuestro servicio, es posible que ofrezcamos suscripciones de prueba pagadas durante un tiempo determinado.</span></p>
<p class="p1"><span class="s1">Al igual que para la suscripción del Servicio CONVOY te solicitaremos datos para abrir tu cuenta e información de pago al comenzar el período de Servicios de Prueba. AL FINALIZAR LA PRUEBA, Y SI NO RECIBIMOS UNA CANCELACIÓN DE TU PARTE </span><span class="s5">AL CORREO administracion@convoynetwork.com Ó A A TRAVÉS DE LAS APLICACIONES DISPONIBLES EN iOS, Android y Web.</span><span class="s1"> SOLICITANDO LA CANCELACIÓN A LA SUSCRIPCION DEL SERVICIO CONVOY, PODREMOS COMENZAR A COBRARTE AUTOMÁTICAMENTE POR LA SUSCRIPCIÓN CONVOY CORRESPONDIENTE EL PRIMER DÍA DESPUÉS DE FINALIZAR EL PERÍODO DE PRUEBA Y SUCESIVAMENTE CADA MES HASTA RECIBIR INSTRUCCIÓN EN CONTRARIO. AL PROPORCIONAR LOS DATOS DE PAGO EN LA PRUEBA, ACEPTAS EL COBRO DEL CARGO.</span></p>
<p class="p1"><span class="s1">SI NO DESEAS QUE TE CONTINUEN COBRANDO POR EL SERVICIO CONVOY ENTONCES DEBERÁS CANCELAR LA SUSCRIPCIÓN PAGADA </span><span class="s5">CORRESPONDIENTE ENVIANDO UN CORREO A LA CUENTA administracion@convoynetwork.com Ó A A TRAVÉS DE LAS APLICACIONES DISPONIBLES EN iOS, Android y Web.</span><span class="s1"> SI LA CANCELACIÓN SE HACE FUERA DEL PLAZO DE COBRO, EL COBRO POR EL MES SIGUIENTE SE REALIZARÁ TOMANDO NOTA DE QUE LA CUENTA SERÁ CANCELADA A LA TERMINACIÓN DEL ÚLTIMO MES PAGADO. LAS SUSCRIPCIONES PAGADAS NO PUEDEN CANCELARSE ANTES DE QUE FINALICE EL PERÍODO QUE YA HAS PAGADO Y, SALVO LO DISPUESTO EN ESTOS T&amp;C, CONVOY NO REALIZARA DEVOLUCIONES DE DINERO A LOS USUARIOS.</span></p>
<p class="p1"><span class="s1"><b>Tus Derechos.</b></span></p>
<p class="p1"><span class="s1">El Servicio CONVOY y todos sus Contenidos son propiedad y/o tiene autorización de sus titulares para ser usados por CONVOY. Mediante el pago de la Suscripción te otorgamos una licencia limitada, no exclusiva y revocable para que utilices el Servicio CONVOY, y una licencia limitada, no exclusiva y revocable para hacer uso personal, no comercial y de entretenimiento del Contenido (la “Licencia”). Esta Licencia tendrá valor hasta que la Suscripción al Servicio CONVOY permanezca vigente o hasta que el Usuario o CONVOY la cancelen.</span></p>
<p class="p1"><span class="s1">El Usuario declara y acepta que usará el Contenido para fines personales, no comerciales y de entretenimiento, y que no redistribuirá ni transferirá el SERVICIO CONVOY ni su Contenido.</span></p>
<p class="p1"><span class="s1">Las aplicaciones de software de CONVOY APP y todo su Contenido se otorgan mediante licencia, por lo que el pago de la Suscripción no deberá entenderse como venta de contenidos.<span class="Apple-converted-space">  </span>CONVOY es el único titular y/o licenciatario autorizado de todas las copias de las aplicaciones de software de CONVOY y su Contenido, incluso después de instalarlas en cualquiera de tus dispositivos personales.</span></p>
<p class="p1"><span class="s1">Todas las marcas registradas de CONVOY, las marcas de servicio, los nombres comerciales, los logotipos, los nombres de dominio y cualquier otra característica de la marca Convoy son propiedad exclusiva de Convoy o se encuentran debidamente licenciadas a favor de ésta última. Los T&amp;C no le conceden al Usuario ningún derecho para usar los derechos de propiedad intelectual propiedad y/o licenciados de o a favor de CONVOY para fines comerciales o no comerciales.</span></p>
<p class="p7"><span class="s1"><b>Derechos que el Usuario le otorga a CONVOY.</b></span></p>
<p class="p8"><span class="s1">En virtud de la Suscripción a nuestros Servicios CONVOY, el Usuario nos otorga los siguientes derechos: (1) permitir que el Servicio CONVOY use el procesador, el ancho de banda y el hardware de almacenamiento de su dispositivo para facilitar el funcionamiento de CONVOY APP, (2) ofrecerle a usted publicidad y otro tipo de información, y (3) permitir que nuestros socios comerciales realicen lo mismo.</span></p>
<p class="p8"><span class="s1">Usted le otorga a CONVOY una licencia no exclusiva, transferible, sublicenciable, sin regalías, indefinida (o en aquellas jurisdicciones que no lo permiten, por el período más amplio permitido), irrevocable, totalmente pagada y universal para usar, reproducir, poner a disposición del público (p. ej., crear o mostrar), publicar, traducir, modificar, crear trabajos derivados y distribuir cualquier Contenido que tu como Usuario hayas subido a CONVOY a través de cualquier medio, ya sea solo o combinado con otros contenidos o material, de cualquier modo y por cualquier medio, método o tecnología, ya sea que existan actualmente o que se creen en un futuro.</span></p>
<p class="p4"><span class="s1"><b>Privacidad.</b></span></p>
<p class="p4"><span class="s1">El Usuario acepta que CONVOY podrá recopilar y tratar la información de carácter personal que obtengamos al momento en que das de alta tu cuenta en nuestro sitio web o en la CONVOY APP, así como los datos bancarios necesarios para el pago de nuestro Servicio CONVOY. Para conocer más sobre nuestra Política de Privacidad te invitamos a conocerla.</span></p>
<p class="p4"><span class="s1"><b>Responsabilidad limitada.</b></span></p>
<p class="p4"><span class="s1">Es importante hacer de tu conocimiento que ni CONVOY ni sus asociadas, altos cargos, consejeros y empleados responderán daños y/o perjuicios directos, indirectos, especiales o consecuentes (incluidos, entre otros, las pérdidas de datos, interrupciones del servicio, fallos informáticos o pérdidas pecuniarias) que surjan con motivo del uso o de la imposibilidad de utilizar el Servicio CONVOY.</span></p>
<p class="p4"><span class="s1">En caso de que una vez pagado el Servicio CONVOY, el sitio o la CONVOY APP no funcionen, te sugerimos ponerte en contacto con nosotros al </span><span class="s5">correo electrónicoadministracion@convoynetwork.com a fin de</span><span class="s1"> que podamos apoyarte con alguna solución o para solicitar la cancelación a la suscripción del Servicio CONVOY.</span></p>
<p class="p1"><span class="s1"><b>Propiedad Intelectual.</b></span></p>
<p class="p1"><span class="s1">Te pedimos por favor respetar los derechos de propiedad intelectual, en este sentido te pedimos abstenerte de:</span></p>
<ul>
<li class="li1"><span class="s1">Copiar, redistribuir, “ripear” (ripping), grabar, transferir, mostrar al público, transmitir o poner a disposición del público alguna parte del servicio CONVOY o su Contenido, o utilizar el servicio CONVOY o su Contenido de alguna otra manera que no esté expresamente permitido en virtud de estos T&amp;C o la ley vigente o que, de alguna manera, infrinja los derechos de propiedad intelectual (como los derechos de autor) del Servicio de Convoy o su Contenido o de alguna parte de este.</span></li>
<li class="li1"><span class="s1">Usar el servicio Convoy para importar o copiar archivos locales a los que NO tengas derecho a importar ni copiar de esta manera.</span></li>
<li class="li1"><span class="s1">Transferir copias de Contenido caché desde un dispositivo autorizado a otro dispositivo por cualquier medio que se realice.</span></li>
<li class="li1"><span class="s1">Usar técnicas de ingeniería inversa, decompilar, desensamblar, modificar o crear obras derivadas basadas en el Servicio CONVOY o en la CONVOY APP, su Contenido o en alguna parte de este, a menos que lo permita la ley.</span></li>
<li class="li1"><span class="s1">Evadir cualquier tecnología utilizada por CONVOY, sus licenciantes o terceros para proteger el Contenido o el Servicio.</span></li>
<li class="li1"><span class="s1">Vender, alquilar, sublicenciar o arrendar alguna parte del Servicio CONVOY o su Contenido.</span></li>
<li class="li1"><span class="s1">Eliminar o alterar cualquier derecho de autor, marca registrada o aviso de propiedad intelectual incluido en el Servicio CONVOY o proporcionado a través de este (a fin de ocultar o modificar las indicaciones de titularidad o la fuente del Contenido).</span></li>
<li class="li1"><span class="s1">Proporcionar su contraseña a otra persona o usar el nombre de usuario y contraseña de otra persona.</span></li>
<li class="li1"><span class="s1">“Rastrear” el Servicio CONVOY o usar medios automatizados (como, por ejemplo, bots, scrapers y spiders) para recopilar información de CONVOY.</span></li>
<li class="li1"><span class="s1">Vender una cuenta de usuario o el Contenido de la CONVOY APP o aceptar alguna compensación o pago monetario o similar para modificar el nombre de una cuenta o el contenido de alguna cuenta o lista de reproducción.</span></li>
</ul>
<p class="p1"><span class="s1">Te pedimos atentamente respetar los derechos de CONVOY, a los propietarios del Contenido y a los otros usuarios del Servicio CONVOY.</span></p>
<p class="p4"><span class="s1">Todo el Contenido del Servicio CONVOY y CONVOY APP está amparado por derechos de reproducción: &#8220;Copyright © 2016.<span class="Apple-converted-space">  </span>CONVOY ENTERTAINMENT, S.A. de C.V. Todos los derechos reservados. Prohibida su reproducción total o parcial&#8221; o por Contenido de terceras partes proveedoras de Contenido que igualmente, se encuentran protegidos por las leyes internacionales de derechos de reproducción.</span></p>
<p class="p1"><span class="s1"><b>Servicios de Mensajes.</b></span></p>
<p class="p1"><span class="s1">Como parte del Servicio CONVOY el Usuario podrá mantener comunicaciones privadas con otros Usuario que así lo permitan a través de mensajes dentro de la CONVOY APP, en este sentido te pedimos NO participar en actividades o conductas que sean:</span></p>
<ul>
<li class="li1"><span class="s1">Ofensivas, abusivas, difamatorias, pornográficas, amenazadoras u obscenas.</span></li>
<li class="li1"><span class="s1">Ilegales, y que tengan como objetivo fomentar o cometer un acto ilegal de cualquier tipo, incluidas las infracciones de los derechos de propiedad intelectual, los derechos de privacidad y/o los derechos de propiedad de CONVOY o de un tercero.</span></li>
<li class="li1"><span class="s1">Incluir tu contraseña o incluir intencionalmente la contraseña de otro usuario o los datos personales de terceros, o tener como objetivo solicitar dichos datos personales.</span></li>
<li class="li1"><span class="s1">Incluir contenido malintencionado como malware, troyanos o virus, o impedir de otro modo el acceso de un usuario al Servicio CONVOY.</span></li>
<li class="li1"><span class="s1">Acosar o intimidar a otros usuarios.</span></li>
<li class="li1"><span class="s1">Usurpar o tergiversar su afiliación con otro usuario, persona o entidad, o resultar de otro modo fraudulenta, falsa, engañosa o errónea.</span></li>
<li class="li1"><span class="s1">Utilizar medios automatizados para promocionar el contenido ficticiamente.</span></li>
<li class="li1"><span class="s1">Incluir actividades comerciales o de ventas, como publicidades, concursos, sorteos o esquemas piramidales, que no están expresamente autorizados por CONVOY.</span></li>
<li class="li1"><span class="s1">Incluir enlaces a referencias o promocionar de otro modo productos o servicios comerciales, salvo que estén expresamente autorizados por CONVOY.</span></li>
<li class="li1"><span class="s1">Interferir con el Servicio CONVOY o de alguna manera perjudicar, manipular, violar o intentar investigar, explorar o examinar vulnerabilidades en el Servicio CONVOY o en la CONVOY APP en los sistemas o redes informáticas de CONVOY, en las reglas de uso o en cualquier componente de seguridad, medida de autenticación de CONVOY o cualquier otra medida de seguridad correspondiente al Servicio CONVOY, el Contenido o a una parte de este.</span></li>
<li class="li1"><span class="s1">Entra en conflicto con los Contratos, según lo determinado por CONVOY.</span></li>
</ul>
<p class="p1"><span class="s1">Llevar a cabo éste tipo de conductas puede provocar la terminación o suspensión inmediata de tu cuenta o suscripción.</span></p>
<p class="p1"><span class="s1">Sea precavido al usar el Servicio CONVOY y lo que comparte en él. El Servicio CONVOY incluye funciones sociales e interactivas, que incluyen la posibilidad de publicar Contenido de usuario, compartir contenidos y hacer que cierta información suya sea de dominio público. Recuerde que la información pública o compartida la pueden usar o volver a compartir otros usuarios en la CONVOY APP o en la web, por lo que le recomendamos que use la CONVOY APP con precaución y tenga en cuenta la configuración de su cuenta. CONVOY no es responsable de su decisión de publicar material en el Servicio CONVOY.</span></p>
<p class="p1"><span class="s1">La contraseña que uses protege tu cuenta de usuario, por lo que debes tomar en cuenta que eres el único responsable de que esta sea segura y secreta. Comprende que usted es responsable del uso que se haga de su nombre de usuario y contraseña en el Servicio CONVOY. Si pierdes o te roban tu nombre de usuario o contraseña, o si crees que alguien accedió a su cuenta sin autorización, </span><span class="s5">notifíquenos de inmediato al correo administracion@convoynetwork.com y cambie su contraseña lo antes posible.</span></p>
<p class="p1"><span class="s1">CONVOY no tiene ninguna responsabilidad sobre los mensajes que se compartan en la CONVOY APP.</span></p>
<p class="p1"><span class="s1"><b>Violación y denuncia de contenido de usuario.</b></span></p>
<p class="p1"><span class="s5">CONVOY respeta los derechos de los propietarios de propiedad intelectual. Si cree que algún Contenido infringe sus derechos de propiedad intelectual, </span><span class="s4">deberá contactar a CONVOY en la siguiente dirección:</span></p>
<p class="p4"><span class="s1"><i>[Nombre del Contacto]</i></span></p>
<p class="p4"><span class="s1"><i>[Dirección de Correo electrónico]</i></span></p>
<p class="p4"><span class="s1">A fin de poder atender a tu petición, la misma debe incluir, al menos, la siguiente información:</span></p>
<ol class="ol1">
<li class="li4"><span class="s1">Datos de contacto del presunto titular afectado. </span>
<ol class="ol2">
<li class="li4"><span class="s1">Nombre completo del Titular Afectado</span></li>
<li class="li4"><span class="s1">Nombre completo de su Representante Legal (en su caso) acompañado del documento que acredite la personalidad para actuar a nombre y representación del titular.</span></li>
<li class="li4"><span class="s1">Dirección de Correo Electrónico del titular afectado y/o representante Legal</span></li>
<li class="li4"><span class="s1">Teléfono de Contacto del titular afectado y/o representante Legal</span></li>
<li class="li4"><span class="s1">Documento que acredite su titularidad sobre la obra(s).</span></li>
</ol>
</li>
</ol>
<ol class="ol1">
<li class="li4"><span class="s1">Descripción de la obra que consideras se ha utilizado </span>
<ol class="ol2">
<li class="li4"><span class="s1">Descripción del contenido de la obra que se desea proteger. En caso de que se trate de más de una obra, describir individualmente a cada una de las obras supuestamente afectadas.</span></li>
<li class="li4"><span class="s1">Descripción del supuesto uso no autorizado de la obra (s).</span></li>
</ol>
</li>
</ol>
<ol class="ol1">
<li class="li4"><span class="s1">Declaración expresa del titular afectado y/o su representante</span>
<ol class="ol2">
<li class="li4"><span class="s1">El Titular afectado por sí o conducto de su representante deberá declarar bajo protesta de decir verdad que: </span>
<ol class="ol3">
<li class="li4"><span class="s1">La obra (s) de su propiedad fue utilizada sin su consentimiento y/o autorización.</span></li>
<li class="li4"><span class="s1">La información proporcionada es correcta y exacta.</span></li>
<li class="li4"><span class="s1">Actúa de buena fe en lo que considera es la defensa de sus derechos.</span></li>
</ol>
</li>
</ol>
</li>
<li class="li4"><span class="s1">Nombre Completo y Firma.</span></li>
</ol>
<p class="p4"><span class="s1">Una vez recibida dicha información CONVOY contactará al Usuario vía correo electrónico a fin de atender y resolver su solicitud.</span></p>
<p class="p1"><span class="s1"><b>El Servicio CONVOY.</b></span></p>
<p class="p1"><span class="s1">CONVOY hará todo lo posible por que el Servicio CONVOY esté siempre en funcionamiento. No obstante, ciertas dificultades técnicas o de mantenimiento ocasionales pueden provocar la interrupción temporal del Servicio. En la medida que la ley lo permita, CONVOY se reserva el derecho de modificar o suspender, periódicamente y en cualquier momento, ya sea de forma permanente o temporal, funciones y características del Servicio CONVOY, con o sin previo aviso, sin obligación frente el Usuario. No obstante lo anterior, si la falta del funcionamiento es permanente CONVOY suspenderá permanentemente los pagos mensuales subsecuentes a la fecha en que dicha dificultad permanente haya ocurrido.</span></p>
<p class="p1"><span class="s1">CONVOY no tiene la obligación de mantener, proporcionar soporte, actualizar ni mejorar el Servicio CONVOY.</span></p>
<p class="p1"><span class="s1"><b>Asistencia al Cliente.</b></span></p>
<p class="p1"><span class="s5">Si necesitas asistencia en temas relacionados con pagos y cuentas envía un correo aadministracion@convoynetwork.com.<span class="Apple-converted-space">  </span></span><span class="s1">Haremos todo lo posible para responder a todas tus consultas en un plazo razonable, pero no podemos asegurar ni garantizar que responderemos a determinadas consultas en un plazo específico ni que podremos responder a todas las consultas de forma satisfactoria.</span></p>
<p class="p1"><span class="s1"><b>Pagos y cancelaciones.</b></span></p>
<p class="p1"><span class="s1">La suscripción a nuestro Servicio CONVOY se puede adquirir mediante pagos mensuales. Salvo que existan fallas permanentes en el funcionamiento del Servicio CONVOY, el Usuario no recibirá ninguna devolución de tu dinero.</span></p>
<p class="p1"><span class="s5">El pago que realices a CONVOY se renovará automáticamente cada mes, a menos que canceles la Suscripción a través deadministracion@convoynetwork.com</span><span class="s1"> antes de que finalice el período del mes pagado, de lo contrario es posible que CONVOY cobre la suscripción mensual del mes siguiente si la cancelación se hace fuera del plazo de pagos correspondiente. La cancelación entrará en vigor el día después del día en que lo solicites.</span></p>
<p class="p1"><span class="s1">CONVOY puede modificar, periódicamente, el precio de las Suscripciones por lo que te informaremos de forma general en el Contenido de CONVOY APP o de forma personal en tu correo las modificaciones a los precios con anticipación y, si corresponde, la forma de aceptar esos cambios. Los cambios de precio entrarán en vigor al comenzar el siguiente período de suscripción, es decir, en el siguiente mes que inicie el nuevo periodo de suscripción. Si sigues usando el Servicio CONVOY después de que el cambio de precio entre en vigor, constituirá una aceptación del nuevo precio. Si no está de acuerdo con el cambio de precio, tienes derecho a rechazar el cambio y cancelar la suscripción al Servicio CONVOY antes de que el cambio de precio entre en vigor. Por lo tanto, asegúrese de leer la notificación del cambio de precio atentamente.</span></p>
<p class="p1"><span class="s1"><b>Cancelación de cuenta</b>.</span></p>
<p class="p13"><span class="s1">Para cancelar tu cuenta te pedimos ingresar en “_________________” y seguir los siguientes pasos:</span></p>
<p class="p13"><span class="s1">1.</span></p>
<p class="p13"><span class="s1">2.</span></p>
<p class="p1"><span class="s5">3.</span></p>
<p class="p1"><span class="s1"><b>Garantía y exención de responsabilidad.</b></span></p>
<p class="p1"><span class="s1">EL SERVICIO CONVOY SE PROPORCIONA “EN EL ESTADO EN EL QUE ESTÁ” Y “SEGÚN DISPONIBILIDAD”, SIN GARANTÍA EXPRESA NI IMPLÍCITA Y SIN NINGÚN TIPO DE CONDICIÓN. EL USUARIO UTILIZA EL SERVICIO CONVOY POR SU CUENTA Y RIESGO.</span></p>
<p class="p1"><span class="s1">NI CONVOY NI LOS PROPIETARIOS DE CONTENIDO GARANTIZAN QUE EL SERVICIO CONVOY NO TIENE MALWARE U OTROS COMPONENTES MALICIOSOS. ADEMÁS, CONVOY NO DECLARA, AVALA, GARANTIZA NI ASUME RESPONSABILIDAD ALGUNA POR APLICACIONES DE TERCEROS, EL CONTENIDO DE APLICACIONES DE TERCEROS (O EL CONTENIDO DE ESTE), EL CONTENIDO DE USUARIO, O CUALQUIER OTRO PRODUCTO O SERVICIO PROMOCIONADO U OFRECIDO POR TERCEROS EN EL SERVICIO CONVOY O A TRAVÉS DE ESTE, O EN CUALQUIER SITIO WEB VINCULADO, O QUE APAREZCA EN CUALQUIER CARTEL O PUBLICIDAD.</span></p>
<p class="p1"><span class="s1">NINGUNA SUGERENCIA O INFORMACIÓN QUE HAYA RECIBIDO POR PARTE DE CONVOY, EL SERVICIO CONVOY O EL CONTENIDO DE LA CONVOY APP, YA SEA POR ESCRITO U ORALMENTE, CONSTITUIRÁ UNA GARANTÍA EN NOMBRE DE CONVOY NI DE SUS AUTORIDADES, ACCIONISTAS, EMPLEADOS, AGENTES, DIRECTORES, SUBSIDIARIAS, FILIALES O LICENCIANTES A ESTOS EFECTOS.</span></p>
<p class="p1"><span class="s1">EN CASO DE PROBLEMAS O INSATISFACCIÓN CON EL SERVICIO CONVOY, TE PEDIMOS DESINSTALAR EL SOFTWARE DE CONVOY Y DEJAR DE USAR EL SERVICIO CONVOY.</span></p>
<p class="p1"><span class="s1">EN NINGÚN CASO CONVOY, SUS AUTORIDADES, ACCIONISTAS, EMPLEADOS, AGENTES, DIRECTORES, SUBSIDIARIAS, FILIALES O LICENCIANTES SE RESPONSABILIZARÁN POR (1) CUALQUIER DAÑO INDIRECTO, ESPECIAL, INCIDENTAL, PUNITIVO, EJEMPLAR O CONSIGUIENTE, (2) POR LA PÉRDIDA DE USO, DATOS, NEGOCIOS O GANANCIAS (DIRECTAS O INDIRECTAS) QUE, EN TODOS LOS CASOS, SEAN RESULTADO DE USAR O NO PODER USAR EL SERVICIO CONVOY, LAS APLICACIONES DE TERCEROS, O EL CONTENIDO DE LAS APLICACIONES DE TERCEROS, INDEPENDIENTEMENTE DEL MARCO LEGAL, SIN IMPORTAR QUE CONVOY HAYA ADVERTIDO LA POSIBILIDAD DE DICHOS DAÑOS, Y AUN CUANDO UNA POSIBLE SOLUCIÓN NO CUMPLA CON SU FIN ESENCIAL, O (3) EL CONJUNTO DE RESPONSABILIDADES POR TODOS LOS RECLAMACIONES RELATIVAS AL SERVICIO DE CONVOY, APLICACIONES DE TERCEROS O AL CONTENIDO DE LAS APLICACIONES DE TERCEROS MÁS ALLÁ DE LOS IMPORTES QUE EL USUARIO HAYA PAGADO A CONVOY DURANTE SU SUSCRIPCION.</span></p>
<p class="p1"><span class="s1"><b>Derechos de terceros.</b></span></p>
<p class="p1"><span class="s1">Si descargaste la aplicación de la tienda App Store de Apple, Inc. (“Apple”) o si usas la aplicación en un dispositivo iOS, aceptas que has leído, comprendido y acepta el siguiente aviso sobre Apple. Este Contrato es entre el Usuario y CONVOY, no así con Apple, y Apple no es responsable del Servicio CONVOY ni del contenido de la CONVOY APP. Apple no tiene obligación de proporcionar ningún servicio de mantenimiento y soporte con respecto al Servicio CONVOY. En caso de que el Servicio no cumpla con las garantías aplicables, podrás notificar a Apple, y Apple podrá reintegrar el precio de compra correspondiente a la CONVOY APP y, en la medida que lo permita la ley vigente, Apple no tendrá ningún tipo de obligación de garantía respecto del Servicio CONVOY.</span></p>
<p class="p1"><span class="s1">Apple no es responsable de (1) reclamaciones sobre responsabilidad civil en cuanto a productos, (2) cualquier reclamación que el Servicio CONVOY no satisfaga conforme a cualquier requisito legal o reglamentario, y (3) reclamaciones que surjan en virtud de leyes de protección del consumidor o alguna legislación similar. Apple no es responsable de investigar, defender, solucionar ni eliminar las reclamaciones de terceros de que el Servicio CONVOY y su posesión y uso de la aplicación infringen los derechos de propiedad intelectual del tercero.</span></p>
<p class="p1"><span class="s1"><b>Cesión.</b></span></p>
<p class="p1"><span class="s1">CONVOY podrá ceder este Contrato o cualquiera de sus partes, y puede delegar cualquiera de sus obligaciones sin tu autorización. Sin embargo, el Usuario no podrá ceder a terceros éste Contratos ni cualquiera de sus partes, como tampoco podrá transmitir o sublicenciar sus derechos adquiridos.</span></p>
<p class="p1"><span class="s1"><b>Indemnización.</b></span></p>
<p class="p1"><span class="s1">En la medida que lo permita la ley vigente, como Usuario aceptas indemnizar y eximir a CONVOY contra todo daño, pérdida y gasto de cualquier índole (incluidos costes y honorarios razonables de abogado) que surjan de lo siguiente: (1) El violar este Contrato, (2) cualquier Contenido de usuario, (3) cualquier actividad en la que participe en o a través del servicio CONVOY, y (4) violar cualquier ley o los derechos de terceros.</span></p>
<p class="p1"><span class="s1"><b>Ley y Jurisdicción aplicable.</b></span></p>
<p class="p4"><span class="s1">En caso de cualquier controversia, el Usuario acepta someterse a la legislación mexicana y la jurisdicción exclusiva de los tribunales mexicanos, renunciando a cualquier otra que pudiese resultar aplicable.</span></p>
<p class="p4"><span class="s1">Copyright © 2016 Convoy Entertainment, S.A. de C.V. Todos los derechos reservados.</span></p>
</p>

         </div>
   </div>
  </div>
</div>
