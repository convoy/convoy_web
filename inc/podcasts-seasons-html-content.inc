<!--Navegación-->
<?php 
require_once(__DIR__ . '/../php-libs/function.ConnDB.php');
require_once('php-libs/function.General.php');
require_once('nav.inc');
require_once('tabs-welcome.inc');
?>


  <div class="container-fluid ESuper">
  <div class="row">
  <div class="col-md-4">
  <div style="width: 100%; height: 100px; float: left"></div>
  <div id="carga_podcast_new">
	<div id="audio-player">
		<div id="buttons">
			<span>
				<button class="PlayerButton" id="ButtonShare"><i class="fa fa-share-alt"></i>&nbsp;</button>
				<button class="CollecButton" id="ButtonCollection">&nbsp;</button>
				<button class="PlayerButton" id="ButtonLike"><i class="fa fa-heart"></i></button>
      </span>
		</div>
<?php
$Podcast = $_GET['P'];
$s = "
SELECT 
	podc_PodcastID,
	podc_Title,
	podc_Description,
	podc_Image,
	podc_Creation,
	podc_Status
FROM
	Podcast
WHERE
	podc_PodcastID='".$Podcast."'
";
$q = $DB->query($s);
$r = $q->fetch_array();
?>
<div style="display:none;" name="<?php echo base64_encode($_SESSION['SessionUserID']); ?>" id="ses"></div>
<div style="display:none;" name="<?php echo  base64_encode('E'); ?>" id="typw"></div>
<?php
    echo '
		<div><!--inicia el podcasts-->
			<div><!--imagen-->
				<img src="img/cover-podcast/'.$r['podc_PodcastID'].'/'.$r['podc_Image'].'" style="width:97%;" "class="img-responsive" id="EpisodeCover">


<video id="Player" poster="" preload="none" style="height:10px; max-height:1px; display:none;" >
  <source type="video/mp4">
Your browser does not support the video tag.
</video>


<!--  <source src="movie.mp4" type="video/mp4" style="height:10px; max-height:10px;">
  <source src="movie.ogg" type="video/ogg">
				<audio id="Player" autoplay="autoplay"></audio>
-->
			</div><!--termina portada-->
			<div>
			
			</div>
			
		</div><!--Termina el podcasts-->

    <div id="audio-info">
      <span class="artist"></span><br />
      <span class="title"></span>
    </div>
     <!--<input id="volume" type="range" min="0" max="10" value="5" />-->

    <!-- PLAYER BAR -->
    <div class="progressContainer">
    <progress id="seekbar" value="0" max="1"></progress>
    <input type="range" step="any" id="seekbardos">
    </div>
    <!-- // PLAYER BAR -->
     <br>
     <div id="buttons">
			<span>
				<button class="PlayerButton" id="ButtonPrev" disabled><i class="fa fa-backward"></i></button>
				<button class="PlayerButton" id="ButtonPlay"><i class="fa fa-play"></i></button>
				<button class="PlayerButton" id="ButtonNext"><i class="fa fa-forward"></i></button>
      </span>
     </div>
     <div class="clearfix"></div>
     <div id="tracker">
      <div id="progressBar">
        <span id="progress"></span>
      </div>
      <span id="duration"></span>
     </div>
	</div>

	';

?>
<script>
$('#play').on('click', function() {
document.getElementById('Player').play();
});

$('#pause').on('click', function() {
document.getElementById('Player').pause();
});

$('#Player').on('timeupdate', function() {
$('#seekbar').attr("value", this.currentTime / this.duration);
});

$('#seekbardos').mouseup(function() {
document.getElementById('Player').play();
});

$('#seekbardos').mousedown(function() {
document.getElementById('Player').pause();
});

var audio = document.getElementById('Player');
var seekbar = document.getElementById('seekbardos');
document.getElementById('seekbardos').value = '0';
function updateUI(){
    var lastBuffered = audio.buffered.end(audio.buffered.length-1);
    seekbar.min = audio.startTime;
    seekbar.max = lastBuffered;
    seekbar.value = audio.currentTime;
}
function setupSeekbar(){
    seekbar.min = audio.startTime;
    seekbar.max = audio.startTime + audio.duration;
}
function seekAudio(){
    audio.currentTime = seekbar.value;
}


seekbar.onchange = seekAudio;
audio.ontimeupdate = updateUI;
audio.ondurationchange = setupSeekbar;
//var lastBuffered = audio.buffered.end(audio.buffered.length-1);


</script>
</div>
   </div>

  <div class="col-md-4" style="background-color:#333;">
  <div style="width: 100%; float: left"></div>
  <div id="carga_podcast_new">
   <ul id="playlist">


<?php

$sS = "
SELECT
	seas_SeasonID,
	seas_Title,
	seas_Description,
	seas_Image,
	seas_Creation,
	seas_Status,
	epis_EpisodeID,
	epis_SeasonID,
	epis_Episode,
	epis_Title,
	TIME_FORMAT(epis_Length, '%H') AS LengthHrs,
	TIME_FORMAT(epis_Length, '%i') AS LengthMin,
	TIME_FORMAT(epis_Length, '%s') AS LengthSec,
	epis_Creation,
	epis_Status


FROM 
	Podcast_Episodes
	INNER JOIN 
	Podcast_Season ON seas_SeasonID=epis_SeasonID
	
WHERE
	seas_PodcastID ='".$Podcast."'
	AND epis_PlayDate<='".date('Y-m-d H:is')."'

	/*ORDER BY seas_Title, epis_PlayDate DESC*/
    ORDER BY epis_PlayDate DESC
";
$qS = $DB->query($sS);
while($rS = $qS->fetch_array()){
	$Lenght = '';
	if($rS['LengthHrs']!='00'){$Lenght.=$rS['LengthHrs'].':';}
	$Lenght.= $rS['LengthMin'].':'.$rS['LengthSec'];

echo '<li id="'.$rS['epis_EpisodeID'].'" class="Episode"><div class="pocEpisTitle">'.$rS['seas_Title'].' | '.$rS['epis_Title'].'</div><div class="pocdEpisNo">Episodio: '.$rS['epis_Episode'].'</div><i class="fa fa-info-circle"></i></span> <span class="lenght">'.$Lenght.'</span></li>';
	}


$DB->close();
?>
  		</ul>
		</div>
	</div>

  <div class="col-md-4">
    <div style="width: 100%; height:7em;"></div>
      <div id="Loader" style="color:#ffffff;">
  		<?php
      echo '<h1>'.$r['podc_Title'].'</h1>';
      echo '<p>'.$r['podc_Description'].'</p>';
			?>
  		</div>
  	</div>
  </div>

  </div>

</div>
<div>
<script type="text/javascript" src="js/like.js"></script>