function validate(){
		
    var Msg = "";
        
    var password1 = $('input[name=NewPass]').val().trim();
    var password2 = $('input[name=NewPassRepeat]').val().trim();
    var email = $('input[name=email]').val().trim();
    var token = $('input[name=token]').val().trim();
    
    var data2 =
    {
        "Email":email,
        "NewPass":password1,
        "Token":token
    }
    
    if(password1 != password2){
        alert('Las contraseñas no coinciden');
        return false;
    }else{
        
        $.ajax({
               type: "POST",
               dataType:"json",
               beforeSend: function(xhr){
               xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
               },

               url: 'https://www.convoynetwork.com/ws/password-reset-web-2.php',
               data: data2,
               success: function(result){
               //alert("Tu contraseña se ha reestablecido exitosamente");
               },
               error: function(result){
               //alert("Hubieron problemas al reestablecer tu contraseña. Intenta nuevamente.");
               var items = [];
               $.each( result, function( key, val ) {
                      items.push( console.log(val) );
                      });
               
               },
               complete:function(){alert("Tu contraseña ha sido reestablecida.");}
               
               });
        
        
        
    }

    
    
}