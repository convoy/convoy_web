
$("#mensaje_ctrl").submit(function(event) { 
// Stop form from submitting normally
event.preventDefault();
 
// Get some values from elements on the page:
var $form = $( this ),
    id_usuario_origen = $form.find("input[name='id_usuario_origen']" ).val(),
    id_usuario_destino = $form.find("input[name='id_usuario_destino']").val(),
    id_mensaje_padre = $form.find("input[name='id_mensaje_padre']").val(),
    mensaje = $form.find("input[name='mensaje']").val(), 
    url = 'http://convoynetwork.com/convoy-webapp/proc/guardar_mensaje.php';
 
  // Send the data using post
  var posting = $.post( url, { 
    id_usuario_origen: id_usuario_origen,
    id_usuario_destino: id_usuario_destino,
    id_mensaje_padre: id_mensaje_padre,
    mensaje : mensaje } );
 
  // Put the results in a div
  posting.done(function( data ) { 
    // var content = $( data ).find( "#content" );
    $( "#mensaje_res" ).empty().load('http://convoynetwork.com/convoy-webapp/proc/mensajes_leer.php');

});
});