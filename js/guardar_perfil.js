
$("#guardar_perfil").submit(function(event) { 
// Stop form from submitting normally
event.preventDefault();
 
// Get some values from elements on the page:
var $form = $( this ), 
    userfile = $form.find("input[name='userfile']").val(),
    descripcion = $form.find("textarea[name='descripcion']").val(),
    website = $form.find("input[name='website']" ).val(),
    instagram = $form.find("input[name='instagram']").val(),
    facebook = $form.find("input[name='facebook']").val(),
    twitter = $form.find("input[name='twitter']").val(),
    tipo_cuenta = $form.find("input[name='tipo_cuenta']").val(), 
    url = 'http://convoynetwork.com/convoy-webapp/proc/actualizar_usuario.php';
 
  // Send the data using post
  var posting = $.post( url, { 
    userfile: userfile,
    descripcion: descripcion,
    website: website,
    instagram: instagram,
    facebook: facebook,
    twitter: twitter,
    tipo_cuenta: tipo_cuenta
     } );
 
  // Put the results in a div
  posting.done(function( data ) { 
    var content = $( data ).find( "#content_per" );
    $( "#resultado_perfil" ).empty().load('http://convoynetwork.com/convoy-webapp/proc/mensaje_guardado.php');

});
});