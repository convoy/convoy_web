/**
 * OpenPay Stuff
 */

// var defaultUrl = window.location.protocol + "//" + window.location.host + "/ws/",
var defaultUrl = (typeof baseUrl === "undefined") ? "./" : baseUrl + "/ws/";
var apiKey = "ac2ee9536910fa6cea18ebaf294467a8", openpayUserId, openpayCardId, openpaySubscriptionId, cardNumber,
merchantId = "msterkwozwegqzaeowsu",
openPayPublic = "pk_7ea5c83b97754e1dafc70945872b6898";
var dots = "&#8226;&#8226;&#8226;&#8226;";
var emptyCard = dots + " " + dots + " " + dots + " " + dots;

OpenPay.setId(merchantId);
OpenPay.setApiKey(openPayPublic);
OpenPay.setSandboxMode(false); //remove for Production

console.info("is Sandbox?", OpenPay.getSandboxMode());

count = 0;


// Initiating
function getCustomerId() 
{
	if (typeof globalUserId === "undefined") {
		globalUserId = null;
	} 
	console.log("UserID:", globalUserId);
	console.log("page", page);
	console.log("url", baseUrl + "/convoy-webapp/php-libs/enable-me.php");
	//count++;
    //Stop this from happening with "2"
	// if (count <= 2) {
		// menu-secundario
		$('body').loader('show', {
            overlay: true
        });
		$.ajax({
			type: "POST",
			url: defaultUrl + 'openpay-check-customer.php',
			data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
			contentType: "application/json",
			beforeSend: function(xhr){
			  xhr.setRequestHeader('Apikey', apiKey);
			},
			success: function(data, status, xhr ) {
				if (data.AppStatus == 0) {
					console.log("no CustomerId");
					$("p.ActualizarTarjeta span:last").html(emptyCard);

					//Let's create the new user
					$.ajax({
						type: "POST",
						url: defaultUrl + 'openpay-new-customer.php',
						data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
						contentType: "application/json",
						beforeSend: function(xhr){
						  xhr.setRequestHeader('Apikey', apiKey);
						},
						success: function(data, status, xhr ) {
							//Setting the Customer ID
							if (data.AppStatus != 0) {
								openpayUserId = data.UserOpenPayId;
								console.log('openpayUserId', openpayUserId);
							} else {
								alert(data.AppResponse);
							}
							
							$('body').loader('hide');
						},
						error: function(xhr, status, error) {
							console.error(error);
						}
					});

				} else {
					//Setting the Customer ID
					openpayUserId = data.UserOpenPayId;
					console.log('openpayUserId', openpayUserId);
					$('body').loader('hide');
				}

			},
			error: function(jqXHR, textStatus, errorThrown) {
			  console.error(jqXHR.responseText);
			  alert("Ocurrio un error al obtener el Suscriptor");
			  $('body').loader('hide');
			}
		});
	// }
};

$(document).ready(function(){
	// Initiating
	$(".MenuSettingInterior #lst2").on("click", function() {
		console.log("UserID:", globalUserId);
		count++;
       //Stop this from happening with "2"
		if (count <= 2) {
			// menu-secundario
			$('body').loader('show', {
                        overlay: true
                    });
			$.ajax({
				type: "POST",
				url: defaultUrl + 'openpay-check-customer.php',
				data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
				contentType: "application/json",
				beforeSend: function(xhr){
				  xhr.setRequestHeader('Apikey', apiKey);
				},
				success: function(data, status, xhr ) {
					if (data.AppStatus == 0) {
						console.log("no CustomerId");
						$("p.ActualizarTarjeta span:last").html(emptyCard);

						//Let's create the new user
						$.ajax({
							type: "POST",
							url: defaultUrl + 'openpay-new-customer.php',
							data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
							contentType: "application/json",
							beforeSend: function(xhr){
							  xhr.setRequestHeader('Apikey', apiKey);
							},
							success: function(data, status, xhr ) {
								//Setting the Customer ID
								if (data.AppStatus != 0) {
									openpayUserId = data.UserOpenPayId;	
								} else {
									alert(data.AppResponse);
								}
								
								$('body').loader('hide');
							},
							error: function(xhr, status, error) {
								console.error(error);
							}
						});

					} else {
						console.log("CustomerId:", data.UserOpenPayId);
						//Setting the Customer ID
						openpayUserId = data.UserOpenPayId;
						$.ajax({
							type: "POST",
							url: defaultUrl + 'openpay-check-customer-cards.php',
							data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
							contentType: "application/json",
							beforeSend: function(xhr){
							  xhr.setRequestHeader('Apikey', apiKey);
							},
							success: function(cardData, status, xhr ) {
								if (cardData.AppStatus == 0) {
									$("p.ActualizarTarjeta span:last").html(emptyCard);
									console.log(cardData.AppResponse);
								} else {
									console.log("Card:", cardData.CardID);
									// Setting the Customer Card ID
									openpayCardId = cardData.CardID;
									$("p.ActualizarTarjeta span:last").html(cardData.card_number);
								}

								// Check if it's subscribed to something
								$.ajax({
									type: "POST",
									url: defaultUrl + "openpay-check-subscription-status.php",
									data: JSON.stringify({"UserID": globalUserId, "isWeb":1}),
									contentType: "application/json",
									beforeSend: function(xhr){
									  xhr.setRequestHeader('Apikey', apiKey);
									},
									success: function(data, status, xhr ) {
										//Setting the SubscriptionId if it's the case
										if (data.AppStatus != 0) {
											openpaySubscriptionId = data.SubscriptionID;
											console.info("subscription data", data);
											openpayCardId = data.subscriptionData.card.id;
											$("p.ActualizarTarjeta span:last").html(data.subscriptionData.card.card_number);
										} else {
											alert(data.AppResponse);
										}
										$('body').loader('hide');
									},
									error: function(xhr, status, error) {
										console.error(error);
									}
								});

							},
							error: function(xhr, status, error) {
								console.error(error);
							}
						});
					}

					if (cardNumber) {
						$("p.ActualizarTarjeta span:last").html(cardNumber);
					}

				},
				error: function(xhr, status, error) {
				  console.error(error);
				}
			});
		}
	});

	//Updating Card
	$("#pay-button").on("click", function(e){
		e.preventDefault();
		console.log("CustomerID:", openpayUserId);
		$('body').loader('show');
		OpenPay.token.extractFormAndCreate(
			$('#payment-form'),
			function(response){
				console.log("Card Success:", response);
				openpayCardId = response.data.id;
				cardNumber = response.data.card.card_number;
				$("p.ActualizarTarjeta span:last").html(cardNumber);

				if (openpaySubscriptionId == null || openpaySubscriptionId == "") {
					var url = "openpay-customer-subscription.php",
					params = {"UserID": globalUserId, "CardID": openpayCardId};
				} else {
					var url = "openpay-update-subscription.php",
					params = {"UserID": globalUserId, "SourceID": openpayCardId};
				}

				params.isWeb = 1;

				// subscribe the customer or update subscription
				// with the card if cardId exits
				if (openpayCardId != null && openpayCardId != '') {
					$.ajax({
						type: "POST",
						url: defaultUrl + url,
						data: JSON.stringify(params),
						contentType: "application/json",
						beforeSend: function(xhr){
						  xhr.setRequestHeader('Apikey', apiKey);
						},
						success: function(data, status, xhr ) {
							console.info("card data", data);
							if (data.AppStatus == 0) {
								// $("p.ActualizarTarjeta span:last").html(".... .... ....");
								alert(data.AppResponse);
								 $('body').loader('hide');
							} else {
								// Setting the Customer Subscription
								openpaySubscriptionId = data.SubscriptionID;
								alert(data.AppResponse);
								$('body').loader('hide');
								if (page == "open-pay") {
									window.location = baseUrl + "/convoy-webapp/php-libs/enable-me.php";
								}
								// $("p.ActualizarTarjeta span:last").html(cardNumber);
							}
						},
						error: function(xhr, status, error) {
							console.error(xhr, status, error);
								$('body').loader('hide');
						}
					});
				}
			},
			function (error) {
				var content = '', results = document.getElementById('resultDetail');
			    content += 'Estatus del error: ' + error.data.status + '<br />';
			    content += 'Error: ' + error.message + '<br />';
			    content += 'Descripción: ' + error.data.description + '<br />';
			    content += 'ID de la petición: ' + error.data.request_id + '<br />';
			    alert('Fallo en la transacción \n ' + content);
			    $('body').loader('hide');
			},
			openpayUserId
		);
	});

	$("#ConfirmCancelSubscription").on("click", function(){
		console.log("trying to cancel");
		$('body').loader('show');
		$.ajax({
			type: "POST",
			url: defaultUrl + "openpay-cancel-subscription.php",
			data: JSON.stringify({"UserID": globalUserId, "Reason": $("#CancelReason").val(), "isWeb":1}),
			contentType: "application/json",
			beforeSend: function(xhr){
			  xhr.setRequestHeader('Apikey', apiKey);
			},
			success: function(data, status, xhr ) {
				if (data.AppStatus == 0) {
					// $("p.ActualizarTarjeta span:last").html(".... .... ....");
					alert(data.AppResponse);
				} else {
					// Setting the Customer Subscription
					openpaySubscriptionId = openpayCardId = cardNumber =  null;
					$("p.ActualizarTarjeta span:last").html(emptyCard);
					alert(data.AppResponse);
					$('body').loader('hide');
				}
			},
			error: function(xhr, status, error) {
				console.error(error);
$('body').loader('hide');
			}
		});
	});
});

var getOpenPayCard = function getOpenPayCard (openpayUser) {

}
