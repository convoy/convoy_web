$(".tabsTitulos").click(function(event){
	$('#Followers, #Following').removeClass('activa');
	event.preventDefault();
	var Option = $(this).attr('id');
	switch(Option){
		case 'Activities':
			var TabFile ='inc/activity-html-content.php';
			break;
		case 'Collections':
			var TabFile ='inc/colecciones-html-content.php';
			break;
		case 'Messages':
			var TabFile ='inc/messages-html-content.php';
			break;
		}
	$.get(TabFile, function(result){
		$("#TabContent").show();
		$("#TabContent").html(result);
		});
	});

$('body').on('click', '#searchFollowers', function() {
    var UserViewing = parseInt($("#uId").val());
    var userId = $(this).data('id');

  $.ajax({
    type: "POST",
    url: baseUrl + '/ws/followers-showall.php',
    data: JSON.stringify({UserID: userId, UserViewingID: UserViewing}),
    contentType: "application/json",
    beforeSend: function(xhr){
      xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
    },
    success: function(data, status, xhr ) {
      console.log(data);
      var html = '';
      if(data.Followers.length > 0)
      {
        var length = data.Followers.length;
        for (var i = 0 ; i < length ; i++) {
          html += '<li class="fol" style="padding:0px; height:50px;">' +
            '<img class="FollowerImage" src="' + data.Followers[i].Avatar + '">' +
            '<span style="color:#000; padding-left:5px;">' + data.Followers[i].Username + '</span>' +
            '<span class="CharlaIcono pull-right">' +
            '<img src="img/iconos/pafaicon.png" style="width:50px; height:50px;" class="linkmessage2" id="' + data.Followers[i].UserID + '">' +
            '</span>' +
          '</li>';
        }
        
      }
      if(html != '')
      {
        $('#searchTabContent').show();
        $('#searchTabContent').html('<ul class="activity felps">' + html + '</ul>');
      }
    },
    error: function(xhr, status, error) {
      //console.log(error);
    }
  });
});

$('body').on('click', '#searchFollowing', function() {
    var UserViewing = parseInt($("#uId").val());
    var userId = $(this).data('id');

  $.ajax({
    type: "POST",
    url: baseUrl + '/ws/following-showall.php',
    data: JSON.stringify({UserID: userId, UserViewingID: UserViewing}),
    contentType: "application/json",
    beforeSend: function(xhr){
      xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
    },
    success: function(data, status, xhr ) {
      console.log(data);
      var html = '';
      if(data.Following.length > 0)
      {
        var length = data.Following.length;
        for (var i = 0 ; i < length ; i++) {
          html += '<li class="fol" style="padding:0px; height:50px;">' +
            '<img class="FollowerImage" src="' + data.Following[i].Avatar + '">' +
            '<span style="color:#000; padding-left:5px;">' + data.Following[i].Username + '</span>' +
            '<span class="CharlaIcono pull-right">' +
            '<img src="img/iconos/pafaicon.png" style="width:50px; height:50px;" class="linkmessage2" id="' + data.Following[i].UserID + '">' +
            '</span>' +
          '</li>';
        }
      }
      if(html != '')
      {
        $('#searchTabContent').show();
        $('#searchTabContent').html('<ul class="activity felps">' + html + '</ul>');
      }
    },
    error: function(xhr, status, error) {
      //console.log(error);
    }
  });
});

$('#Followers').on('click', function(e){
	$('.tabs li').removeClass('activa');
	$('#Following').removeClass('activa');
	$(this).addClass('activa');
	e.stopImmediatePropagation();
	$.post('inc/followers-html-content.php', function(result){
		$("#TabContent").show();
		$("#TabContent").html(result);
		$.getScript('js/app-tabs.js');
		});
	});

$('#Following').on('click', function(e) {
	$('.tabs li').removeClass('activa');
	$('#Followers').removeClass('activa');
	$(this).addClass('activa');
	e.stopImmediatePropagation();
	$.post('inc/following-html-content.php', function(result){
		$("#TabContent").show();
		$("#TabContent").html(result);
		$.getScript('js/app-tabs.js');
		});
	});

$(".linkmessage").on('click', function(e) {
	var ContactoID = $(this).attr('id');
	e.stopImmediatePropagation();
	$.post('inc/messages-conversation-html-content.php', {ContactoID:ContactoID}, function(result){
		$("#TabContent").show();
		$("#TabContent").html(result);
		$.getScript('js/app-tabs.js');
		});
	});

$('#SearchTerm').bind('keypress', function( e ) {
  var code = e.keyCode || e.which;
  if(code == 13) {
    $("#ButtonSearch").click();
  }
});

$('body').on('click', '#followItemBtn', function () {
    var userId = $("#uId").val();
    var followId = $(this).data('id');
    var classes = $(this).attr('class');
    var that = $(this);
    classes = classes.split(' ');
    
    if(classes[1] == 'grayPersonIcon')
    {
      $.ajax({
        type: "POST",
        url: baseUrl + '/ws/followers-follow.php',
        data: JSON.stringify({UserID: userId, FollowID:followId}),
        contentType: "application/json",
        beforeSend: function(xhr){
          xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
        },
        success: function(data, status, xhr ) {
          if(data.AppStatus == 1 && data.Following == 1)
          {
            $(that).removeClass('grayPersonIcon');
            $(that).addClass('bluePersonIcon');
          }
        },
        error: function(xhr, status, error) {
          //console.log(error);
        }
      });

    }
    else
    {
      $.ajax({
        type: "POST",
        url: baseUrl + '/ws/followers-unfollow.php',
        data: JSON.stringify({UserID: userId, FollowID:followId}),
        contentType: "application/json",
        beforeSend: function(xhr){
          xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
        },
        success: function(data, status, xhr ) {
          if(data.AppStatus == 1 && data.Following == 0)
          {
            $(that).removeClass('bluePersonIcon');
            $(that).addClass('grayPersonIcon');
          }
        },
        error: function(xhr, status, error) {
          //console.log(error);
        }
      });

    }

});

$('#SearchUsers').on('click', function () {
   $('label[for="SearchUsers"]').addClass('selectedSearchButton');
   $('label[for="SearchContent"]').removeClass('selectedSearchButton');
});

$('#SearchContent').on('click', function () {
   $('label[for="SearchContent"]').addClass('selectedSearchButton');
   $('label[for="SearchUsers"]').removeClass('selectedSearchButton');
});

$("#ButtonSearch").click(function(){
	var SearchTerm = $("#SearchTerm").val();
	var SearchFor = $(".SearchFor:checked").val();
  var userId = $("#uId").val();
    $('#SearchResults').show();
  $.ajax({
    type: "POST",
    url: baseUrl + '/ws/search.php',
    data: JSON.stringify({UserID: userId, Search:SearchTerm, Type:SearchFor}),
    contentType: "application/json",
    beforeSend: function(xhr){
      xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
    },
    success: function(data, status, xhr ) {

      var html = '';
      if(SearchFor == 'C' && data && data.AppStatus == 1)
      {
        var length = data.Search.length;

        for(var i = 0 ; i < length ; i++)
        {
          switch(data.Search[i].Type)
          {
            case 'P':
              html += '<li class="searchItem"><a href="podcasts-seasons?P=' + data.Search[i].ID + '">' +
                '<img src="' + data.Search[i].Image + '" class="searchAvatar" height="55" width="55" />' +
                '<strong>' + data.Search[i].Title + '</strong>' +
              '</a></li>';
            break;

            case 'N':
              html += '<li class="searchItem"><a href="noticias?NewsID=' + data.Search[i].ID + '">' +
                '<img src="' + data.Search[i].Image + '" class="searchAvatar" height="55" width="55" />' +
                '<strong>' + data.Search[i].Title + '</strong>' +
              '</a></li>';
            break;
          }
        }
      }

      if(SearchFor == 'U' && data && data.AppStatus == 1)
      {
        var length = data.Usuarios.length;
        var html = '';

        for(var i = 0 ; i < length ; i++)
        {
          var followingClass = 'grayPersonIcon';
          if(data.Usuarios[i].Following == 1)
          {
            followingClass = 'bluePersonIcon';
          }
          html += '<li class="searchItem"><a class="showUserInfo" data-id="' + data.Usuarios[i].UserID + '">' +
                '<img src="' + data.Usuarios[i].Avatar + '" class="searchAvatar" height="55" width="55" />' +
                '<strong>' + data.Usuarios[i].Login + '</strong></a>' +
                '<span class="CharlaIcono pull-right">' + 
                  '<span id="followItemBtn" data-id="' + data.Usuarios[i].UserID + '" class="follow ' + followingClass + '"></span>' +
                '</span>' +
              '</li>';
        }
      }

      if(html)
      {
        $('#searchProfileResult').hide();
        $('#SearchResults').show();
        $('#SearchResults').html(html);
      }

    },
    error: function(xhr, status, error) {
      //console.log(error);
    }
  });

});

$('body').on('click', '#searchActivities', function(){
    var userId = parseInt($("#uId").val());
    var login = $(this).data('login');
    var followId = $(this).data('id');

    $.ajax({
      type: "POST",
      url: baseUrl + '/ws/followers-common.php',
      data: JSON.stringify({"UserID":userId,"FollowID":followId}),
      contentType: "application/json",
      beforeSend: function(xhr){
        xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
      },
      success: function(data, status, xhr ) {

        html = '';
        if(data.episodes.length > 0 )
        {
          for(var i = 0 ; i < data.episodes.length ; i++)
          {
            html += '<li class="searchItem"><img src="' + data.episodes[i].Image + '" ' +
              'width="50" height="50" /> a ' + 
              login + ' le gusto ' + data.episodes[i].Title + '</li>';
          }
        }

        if(data.news.length > 0 )
        {
          for(var i = 0 ; i < data.news.length ; i++)
          {
            html += '<li class="searchItem"><img src="' + data.news[i].Image + '" ' +
              'width="50" height="50" /> a ' +
              login + ' le gusto ' + data.news[i].Title + '</li>';
          }
        }

        if(data.radio.length > 0 )
        {
          for(var i = 0 ; i < data.radio.length ; i++)
          {
            html += '<li class="searchItem"><img src="' + data.radio[i].Image + '" ' +
              'width="50" height="50" /> a ' + 
              login + ' le gusto ' + data.radio[i].Title + '</li>';
          }
        }   
        $('#searchTabContent').show();     
        $('#searchTabContent').html(html);
      },
      error: function(xhr, status, error) {
        console.log(error);
      }
  });
});

$('body').on('click','.showUserInfo', function(){
  var UserViewing = parseInt($("#uId").val());
  var userId = $(this).data('id');

  $.ajax({
    type: "POST",
    url: baseUrl + '/ws/user-info.php',
    data: JSON.stringify({UserID: userId, UserViewing: UserViewing}),
    contentType: "application/json",
    beforeSend: function(xhr){
      xhr.setRequestHeader('Apikey', "ac2ee9536910fa6cea18ebaf294467a8");
    },
    success: function(data, status, xhr ) {
      console.log(data);
      var html = '';

      var sensitiveInfo = [];
      sensitiveInfo[0] = '';
      sensitiveInfo[1] = '';

      if(data.ProfileType == 'P')
      {
        sensitiveInfo[0] = ((data.Website != '')? '<a href="' + data.Website + '" title="Website" target="_blank"><img  class="ImgLinkIconos" src="'+baseUrl+'/convoy-webapp/img/iconos/link.png" alt="Website"></a>' : '') +
            ((data.Instagram != '')? '<a href="' + data.Instagram + '" title="Instagram" target="_blank"> <img class="ImgLinkIconos"  src="'+baseUrl+'/convoy-webapp/img/iconos/instagram.png" alt="Instagram"></a>' : '') +
            ((data.Facebook != '')? '<a href="' + data.Facebook + '" title="Facebook" target="_blank"> <img class="ImgLinkIconos"  src="'+baseUrl+'/convoy-webapp/img/iconos/facebook.png" alt="Facebook"></a>' : '') +
            ((data.Twitter != '')? '<a href="' + data.Twitter + '" title="Twitter" target="_blank"> <img class="ImgLinkIconos"  src="'+baseUrl+'/convoy-webapp/img/iconos/twitter.png" alt="Twitter"></a>' : '');
        sensitiveInfo[1] = '<div id="tabs" class="tabs-info-2">' +
            '<ul class="tabs">' +
              '<li class="activa">' +
                '<a class="text-uppercase tabsTitulos" id="searchActivities" data-login="'+data.Login+'" data-id="'+data.UserID+'">' +
                  '<svg class="iconsMenuTabs" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">' +
                    '<path d="M43.314,24.911h-4.463L34.633,13.19c-0.248-0.743-0.99-1.241-1.734-1.241c-0.742,0-1.428,0.498-1.672,1.241l-5.954,18.914' +
                      'l-6.014-13.582c-0.249-0.62-0.807-0.991-1.489-1.053c-0.682-0.061-1.302,0.249-1.673,0.807l-4.402,6.635H6.356' +
                      'c-0.992,0-1.8,0.807-1.8,1.799s0.808,1.799,1.8,1.799h6.262c0.62,0,1.179-0.31,1.489-0.807l3.162-4.713l6.635,14.945' +
                      'c0.31,0.682,0.93,1.053,1.611,1.053h0.123c0.743-0.062,1.363-0.559,1.612-1.24l5.771-18.355l2.852,7.874' +
                      'c0.249,0.743,0.932,1.179,1.674,1.179h5.704c0.991,0,1.8-0.807,1.8-1.799C45.114,25.715,44.307,24.911,43.314,24.911"/>' +
                  '</svg>  ' +
                  '<span>Actividad</span>' +
                '</a>' +
              '</li>' +
            '</ul>' +
            '<div id="searchTabContent" class="contenidoTab Interior">' +
              '<!--activity stream -->' +
            '</div>' +
          '</div><!--Termina todos los tabas-->' +
          '<div id="tab-5" class="contenidoTab"><!--Seguidores-->' +
            '<ul class="activity sin-padding">' +
              '<li class="fol"><a href=""><img src="https://convoynetwork.com/convoy-webapp/image-avatar/defaultConvoy.jpg" class="img-responsive" alt="" /><span><i class="iconPodcasts iconPodcastsPersonaAzul"></i><i class="iconPodcasts iconPodcastsPersonaGris"></i><a href="javascript:cambio_tabla(\'#tab-3\')"><img src="https://convoynetwork.com/convoy-webapp/img/iconos/mensajes-azul.svg" style="margin-left: 120px"></a></a></li>' +
            '</ul>' +
          '</div><!--Seguidores-->' +
          '<div id="tab-6" class="contenidoTab">' +
            '<ul class="activity sin-padding"><!--siguiendo-->' +
              '<li class="fol"><a href="#"><img src="https://convoynetwork.com/convoy-webapp/image-avatar/defaultConvoy.jpg" class="img-responsive" alt="" /><i class="iconPodcasts iconPodcastsPersonaAzul"></i><i class="iconPodcasts iconPodcastsPersonaGris"></i></a></li>' +
            '</ul>' +
          '</div><!--Siguiendo-->';
      }

      html = '<img src="' + data.Avatar + '" height="50" width="50" /> <h2>' + data.Login + '</h2>' +
            ((data.Description != '')? '<p class="ContenidoTexto">' + data.Description + '</p>' : '') +
            '<ul class="IconLinkNav">' +
             sensitiveInfo[0] +            
            '<a title="link" id="searchFollowers" class="seguidores text-upprecase"> Seguidores <br>' + data.Followers + '</a>' +
            '<a title="link" id="searchFollowing" class="seguidores text-upprecase"> Siguiendo <br>' + data.Following + '</a>' +
          '</ul>' +
          sensitiveInfo[1];

      if(html != '')
      {
        $('#searchProfileResult').html(html);
        $('#SearchResults').hide();
        // $('#searchProfileResult').show();
        $('#searchProfileResult').show({direction:"left"});        
      }

    },
    error: function(xhr, status, error) {
      console.log(error);
    }
  });
});


(function(){


$('.muestra-noti').hide();

$('.infoBtn').click( function(){
	$('.muestra-noti').show();
	$('.muestra-noti').addClass('animated fadeInDownBig');
	});

$('#closeInfoPodcasts').click(function(){
	$('.muestra-noti').hide('fast');
	})


$('#tabs div').hide();
$('#tabs div:first').show();
$('#tabs ul li:first').addClass('activa');





$("#favoritosPod").on({

   mouseenter: function() {

     $("#favoritosPod .folColectionLink").addClass("active");
    $("#favorite").show();
    $("#favorite").addClass('animated slideInLeft a');
    $("#favorite .folColectionImg").show();
    $("#Historias, #sonido, #btnCrear").hide();

},
   mouseleave: function() {
     $("#favoritosPod .folColectionLink").removeClass("active");
    $("#favorite").hide();
    $("#favorite").removeClass('animated slideInLeft a');
    $("#favorite .folColectionImg").hide();
    $("#Historias, #sonido, #btnCrear").show();

}


});


$("#Historias").on({

 mouseenter: function() {
    $("#Historias .folColectionLink").addClass("active");
    $("#favHistorias").show();
    $("#favHistorias").addClass('animated slideInLeft a');
    $("#favHistorias .folColectionImg").show();
    $("#favoritosPod, #sonido, #btnCrear").hide();

},
   mouseleave: function() {
     $("#Historias .folColectionLink").removeClass("active");
    $("#favHistorias").hide();
    $("#favHistorias").removeClass('animated slideInLeft a');
    $("#favHistorias .folColectionImg").hide();
    $("#favoritosPod, #sonido, #btnCrear").show();

}
});

$("#sonido").on({

   mouseenter: function() {
     $("#sonido .folColectionLink").addClass("active");
    $("#favSonido").show();
    $("#favSonido").addClass('animated slideInLeft a');
    $("#favSonido .folColectionImg").show();
    $("#Historias, #favoritosPod, #btnCrear").hide();

},
   mouseleave: function() {
     $("#sonido .folColectionLink").removeClass("active");
    $("#favSonido").hide();
    $("#favSonido").removeClass('animated slideInLeft a');
    $("#favSonido .folColectionImg").hide();
    $("#Historias, #favoritosPod, #btnCrear").show();

}



});


$('#tabs ul li a').click(function(){
$('#tabs ul li').removeClass('activa');

$(this).parent().addClass('activa');
var currentTab = $(this).attr('href');
$('#tabs div').hide();
$(currentTab).show();
return false;
});





    /*Tabs interior*/

    $('#tab3').click(function(){
         if ($('#tab3').is(':checked') == true){

                $('#list2').hide();
                 $('#list1').hide();
             $('#btnFolder').hide();


          }else{

          $('#list2').show();
              $('#list1').show();
              $('#btnFolder').show();
          }


      });


         $('#tab2').click(function(){
         if ($('#tab2').is(':checked') == true){

                $('#list3').hide();
                 $('#list1').hide();
             $('#btnFolder').hide();

          }else{

          $('#list3').show();
              $('#list1').show();
              $('#btnFolder').show();
          }



      });

          $('#tab1').click(function(){
         if ($('#tab1').is(':checked') == true){

                $('#list2').hide();
                 $('#list3').hide();
             $('#btnFolder').hide();

          }else{

          $('#list2').show();
              $('#list3').show();
              $('#btnFolder').show();
          }



      });


     $('#btnPause').hide();


    $('#btnPlay').mouseup(function(){

        $('#btnPlay').hide();
         $('#btnPause').show();


    })


     $('#btnPause').mouseup(function(){

         $('#btnPlay').show();
         $('#btnPause').hide();




    })

    /*Configuracion*/

        $("#bc20").click(function() {
               if($("#bc20").is(':checked') == true){

                $("#lst1,#lst6,#lst2,#lst4,#lst5").hide();
               }else{
                  $("#lst1,#lst6,#lst2,#lst4,#lst5").show();
               }

            });

     $('#Configura').click(function(){
       $('.MenuSettings').show();
       $('#Configura').addClass('actived');

     });



    $("#bc3").click(function () {
       if($("#bc3").is(':checked') == true){

        $("#lst1,#lst2,#lst3,#lst4,#lst5").hide();
       }else{
          $("#lst1,#lst2,#lst3,#lst4,#lst5").show();
       }

    });


$("#bc4").click(function () {
       if($("#bc4").is(':checked') == true){

        $("#lst1,#lst6,#lst3,#lst4,#lst5").hide();
       }else{
          $("#lst1,#lst6,#lst3,#lst4,#lst5").show();
       }

    });


$('#ActualizarPago').click(function() {
     $("#lst1,#lst6,#lst3,#lst4,#lst5,#lst2").hide();
      $("#bc5").prop("checked", true);
     $("#lst7").show('fast');
});

$('#CancelarSuscripcion').click(function() {
     $("#lst1,#lst6,#lst3,#lst4,#lst5,#lst2").hide();
      $("#bc6").prop("checked", true);
     $("#lst8").show('fast');
});



$("#bc5").click(function () {
 $("#lst7").hide('fast');
 $("#lst2").show('fast');
 $("#lst2").css('display','block');
  $("#lst2").prop('checked', false);
    });

$("#bc6").click(function () {
 $("#lst8").hide('fast');
 $("#lst2").show('fast');
 $("#lst2").css('display','block');
  $("#lst2").prop('checked', false);
    });








   /*Navegacion latera derecha*/
   $('.flipster ').hide();

$('#RadioTab').hide();


           $('#uno').on('click', function(){
             $(this).addClass('activad');
             $('#uno a').css({
                'color':'#fff',


             });
            $('#dos a, #tres a').css({
                'color':'#333',


             });

            $('#tres').removeClass('activad');
            $('#dos').removeClass('activad');
             $('#Podcast').toggleClass('iconPodcastNegro');
$('#Podcast').toggleClass('iconPodcastBlanco');
$('#PodcastTab').show();

$('#Radio').addClass('iconRadioNegro');         $('#Radio').removeClass('iconRadioBlanco');
$('#RadioTab').hide();
               $('#Notific').addClass('iconNotificNegro');
$('#Notific').removeClass('iconNotificBlanco');
$('#NotificTab').hide();





           })


           $('#dos').on('click', function(){
            $(this).addClass('activad');
            $('#dos a').css({
                'color':'#fff',


             });
            $('#uno a,#tres a').css({
                'color':'#333',


             });

            $('#tres').removeClass('activad');
            $('#uno').removeClass('activad');
                             $('#Radio').toggleClass('iconRadioNegro');
$('#Radio').toggleClass('iconRadioBlanco');
$('#RadioTab').show();
$('.flipster').show();
$('#Podcast').addClass('iconPodcastNegro');
$('#Podcast').removeClass('iconPodcastBlanco');
$('#PodcastTab').hide();
               $('#Notific').addClass('iconNotificNegro');
$('#Notific').removeClass('iconNotificBlanco');
$('#NotificTab').hide();


           })

           $('#tres').on('click', function(){
        $(this).addClass('activad');
        $('#tres a').css({
                'color':'#fff',


             });
        $('#dos a, #uno a').css({
                'color':'#333',


             });
        $('#dos').removeClass('activad');
        $('#uno').removeClass('activad');
               $('#Notific').toggleClass('iconNotificNegro');
$('#Notific').toggleClass('iconNotificBlanco');
$('#NotificTab').show();
$('#Radio').addClass('iconRadioNegro');         $('#Radio').removeClass('iconRadioBlanco');
$('#RadioTab').hide();
$('#Podcast').addClass('iconPodcastNegro');
$('#Podcast').removeClass('iconPodcastBlanco');      $('#PodcastTab').hide();

           });



  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if (screen.width<1024) {

  $('#BtnUse1').click(function() {
   $('#Btnctn1').css('left', '-770px');


       if ($('#BtnUser1').is(':checked') == false){
                $('#Btnctn1').css({
                  'transition'   :  'all 0.5s ease',
                  'left'         :  '0px'
                });



       }else{
              $('#Btnctn1').css({
                'transition'   :  'all 0.5s ease',
                'left'         :  '-770px'
              });


       };

  });




}else {

  $('#BtnUse1').click(function() {
   $('#Btnctn1').css('left', '-600px');


       if ($('#BtnUser1').is(':checked') == false){
                $('#Btnctn1').css({
                  'transition'   :  'all 0.5s ease',
                  'left'         :  '0px'
                });



       }else{
              $('#Btnctn1').css({
                'transition'   :  'all 0.5s ease',
                'left'         :  '-600px'
              });


       };

  });



}



if (screen.width<1024) {
  $('#SetLabel1').click(function() {
   $('#set-ctn1').css('right', '-770px');


       if ($('#set1').is(':checked') == false){
                $('#set-ctn1').css({

                  'transition'   :  'all 0.5s ease',
                  'right'         :  '-0px'
                });



       }else{
              $('#set-ctn1').css({

                'transition'   :  'all 0.5s ease',
                'right'         :  '-770px'
              });


       }

  });




}else{

  $('#SetLabel1').click(function() {
   $('#set-ctn1').css('right', '-600px');


       if ($('#set1').is(':checked') == false){
                $('#set-ctn1').css({

                  'transition'   :  'all 0.5s ease',
                  'right'         :  '-0px'
                });



       }else{
              $('#set-ctn1').css({

                'transition'   :  'all 0.5s ease',
                'right'         :  '-600px'
              });


       }

  });




}


if (screen.width<1024) {
  $('#SetLabel2').click(function() {
   $('#set-ctn2').css('right', '-770px');


       if ($('#set2').is(':checked') == false){
                $('#set-ctn2').css({
                  'transition'   :  'all 0.5s ease',
                  'right'         :  '-0px'
                });



       }else{
              $('#set-ctn1').css({
                'transition'   :  'all 0.5s ease',
                'right'         :  '-770px'
              });


       }

  });




}else{


  $('#SetLabel2').click(function() {
   $('#set-ctn2').css('right', '-600px');


       if ($('#set2').is(':checked') == false){
                $('#set-ctn2').css({
                  'transition'   :  'all 0.5s ease',
                  'right'         :  '-0px'
                });



       }else{
              $('#set-ctn1').css({
                'transition'   :  'all 0.5s ease',
                'right'         :  '-600px'
              });


       }

  });




}












})();
