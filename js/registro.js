
$("#reaccion").submit(function(event) { 
// Stop form from submitting normally
event.preventDefault();
 
// Get some values from elements on the page:
var $form = $( this ),
    usuario = $form.find("input[name='usuario']" ).val(),
    password = $form.find("input[name='password']").val(),
    email = $form.find("input[name='email']").val(),
    anio = $form.find("select[name='anio']").val(),
    mes = $form.find("select[name='mes']").val(),
    dia = $form.find("select[name='dia']").val(),
    url = 'http://convoynetwork.com/convoy-webapp/proc/registro.php';
 
  // Send the data using post
  var posting = $.post( url, { 
    usuario: usuario,
    password: password,
    anio: anio,
    mes: mes,
    dia: dia, 
    email: email } );
 
  // Put the results in a div
  posting.done(function( data ) { 
    var content = $( data ).find( "#content" );
    $( "#resultado" ).empty().load('http://convoynetwork.com/convoy-webapp/proc/sesion_registro.php');

});
});