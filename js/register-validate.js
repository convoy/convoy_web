function validate(){
		
    var Msg = "";
        
    var password1 = $('input[name=Psswd]').val().trim();
    var password2 = $('input[name=Psswd_Retype]').val().trim();
    var email1 = $('input[name=Email]').val().trim();
    var email2 = $('input[name=Email_Retype]').val().trim();
    
    if(password1 != password2){
        alert('Las contraseñas no coinciden');
        return false;
    }
    
    if(email1 != email2){
        alert('Los correos no coinciden');
        return false;
    }

}
