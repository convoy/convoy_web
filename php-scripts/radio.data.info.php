<?php
date_default_timezone_set("America/Mexico_City");
require_once('../php-libs/function.ConnDB.php');

if(isset($_GET['IniDate']))
	$RadioID = $_GET['IniDate'];

$s = " SELECT radi_Title, radi_Description, radi_IniDatetime, radi_Image, radi_EndDatetime FROM Radio WHERE radi_RadioID='".$RadioID."'";
$q = $DB->query($s);
$t = $q->num_rows;
if($t>0){
    $r = $q->fetch_array();
    $dateIni =date_create($r['radi_IniDatetime']);
    $dateEnd =date_create($r['radi_EndDatetime']);
    $dateIni = date_format($dateIni,"d/m H:i");
    $dateEnd = date_format($dateEnd,"d/m H:i");
    if($r['radi_Image'] != ''){
        $radioImage = 'img/cover-radio/'.$r['radi_Image'];
    }else{
        $radioImage = 'img/cover-radio/radio_avatar.jpg';
    }
    
    
        echo '
        <div class="radioDesc">
        <div class="closeRadioInfo">Cerrar</div>
        <img src="'.$radioImage.'" onerror="imgError(this);" />
        <h3>'.$r['radi_Title'].'</h3>
        <p>
        Inicio: '.$dateIni.'<br>
        Fin: '.$dateEnd.'<br>
        '.$r['radi_Description'].'</p>
        </div>
        <script>$(".closeRadioInfo").click(function(){$("#LoaderInfo").fadeOut();});</script>
        ';

}else{
    echo "<script> console.log('No results');</script>";
}

?>