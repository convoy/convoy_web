<?php
// Init
define( "DEBUGGING", true ); // or false in production enviroment
// Functions

include './base_url.php';

function get_cache_prevent_string( $always = false ){
	return (DEBUGGING || $always) ? date('_Y-m-d_H:i:s'):"";
	}

$pagesWithoutSession = array(
	'login',
	'registro',
	'restaurar',
	'password-reset',
	'open-pay',
    'io-purchase',
);
		
session_start();
if($_SESSION){
	require_once('php-libs/function.SessionValidate.php');
	require_once('php-libs/function.ConnDB.php');
}

if (function_exists("mb_internal_encoding")) mb_internal_encoding ('utf-8');
	$request_url = $complete_url = isset($_SERVER["HTTP_X_REWRITE_URL"]) ? $_SERVER["HTTP_X_REWRITE_URL"] : $_SERVER["REQUEST_URI"];

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache");


if (preg_match("/^([^\\?]*)\\?.*/i", $request_url, $coincidencias)) $request_url = $coincidencias[1];
$dirs = explode("/", $request_url);
$page = $dirs[count($dirs)-1];
$page = is_null($page) ? "" : $page;

if (!(file_exists("inc/{$page}-php-head.inc") || file_exists("inc/{$page}-html-content.inc"))) $page = "error-404";
if (file_exists("inc/{$page}-php-head.inc")){
	include "inc/{$page}-php-head.inc";
	}

if( (!isset($_SESSION['SessionUserID'])) ){
    //|| $page != 'restaurar'
    if( !in_array($page, $pagesWithoutSession) ){
        header("Location: login");
        exit;
    }
    
} elseif ( !$_SESSION['SessionSuscribed'] && !in_array($page, $pagesWithoutSession)) {
	header('Location: ' . BASE_URL . '/convoy-webapp/open-pay');
}
?>
<!doctype html>
<html lang="es-MX">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				 <!--
		    Formatted for readability
		  -->
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta name="description" content=" ">
<meta http-equiv="Pragma" content="no-cache">



<meta property="og:type" name="og_type" content="article" />
<meta property="og:title" name="og_title" content="CONVOY" />
<meta property="og:description" name="og_title" content="CONVOY" />
<meta property="og:site_name" name="og_title" content="CONVOY" />
<meta property="og:url" name="og_url" content="<?php echo BASE_URL;?>/convoy-webapp/login" />
<meta property="og:image" name="og_image" content="<?php echo BASE_URL;?>/convoy-webapp/img/logo-convoy.png" />

<base href="<?php echo BASE_URL . "/"; ?>convoy-webapp/" />
<title>CONVOY</title>
		<?php if (file_exists("inc/{$page}-html-head.inc")) include "inc/{$page}-html-head.inc"; ?>
		<!-- Bootstrap core CSS -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="icon" type="image/png" href="favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
		<link href="css/bootstrap.min.css?version=3.3.5<?php echo get_cache_prevent_string(); ?>" rel="stylesheet">
		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?version=4.5.0<?php echo get_cache_prevent_string(); ?>"-->
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css?version=4.5.0<?php echo get_cache_prevent_string(); ?>">
	<!-- Custom styles for this template -->

		<link href="css/style.css?version=3.2<?php echo get_cache_prevent_string(); ?>" rel="stylesheet">
		<link rel="stylesheet" href="css/jQuery.loader.css?<?php echo get_cache_prevent_string(); ?>" />
		<link rel="stylesheet" media="(max-width: 990px)" href="css/desktop-990.css?<?php echo get_cache_prevent_string(); ?>" />
		<link rel="stylesheet" media="(max-width: 768px)" href="css/tablet.css?<?php echo get_cache_prevent_string(); ?>" />
		<link rel="stylesheet" media="(max-width: 480px)" href="css/movil.css?<?php echo get_cache_prevent_string(); ?>" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

        <!--script src="js/jquery-1.11.3.js?v=<?php echo get_cache_prevent_string(); ?>"></script-->
        <script src="//code.jquery.com/jquery-latest.js"></script>
</head>

	<body>

<?php
if (file_exists("inc/{$page}-html-content.inc")){
	require_once("inc/{$page}-html-content.inc");
	}
?>


<?php if(isset($_SESSION['SessionUserID'])) : ?>
<script type="text/javascript">
	var globalUserId = "<?php echo $_SESSION['SessionUserID']; ?>";
	var baseUrl = "<?php echo BASE_URL; ?>";
	var page = "<?php echo $page; ?>";
	// baseUrl = window.location.protocol + "//" + window.location.host;
</script>
<?php endif; ?>

<script src="js/bootstrap.js?v=<?php echo get_cache_prevent_string(); ?>"></script>

<script type="text/javascript" src="js/app-tabs.js?v=<?php echo get_cache_prevent_string(); ?>"></script>

<!--
<script type="text/javascript" src="js/audio.js"></script>
-->
<script type="text/javascript" src="js/app.js?v=<?php echo get_cache_prevent_string(); ?>"></script>
<script type="text/javascript" src="js/clipboard.min.js?v=<?php echo get_cache_prevent_string(); ?>"></script>
<script src="js/jquery.flipster.js?v=<?php echo get_cache_prevent_string(); ?>"></script>
<?php
    if($page=='radio'){
?>
<script>
    //$('#Mejor').flipster();
    $(function(){ $(".flipster").flipster(); });
</script>
</body>
</html>
<?php } //isset session userid ?>
<script>
$('a[href]').each(function(){
    var title = $(this).attr('title');
    if( title != "Website" &&
        title != "Instagram" &&
        title != "Facebook" &&
        title != "Twitter" &&
        title != "newsContent"){
        u = $(this).attr('href');
        $(this).removeAttr('href').data('href',u).click(function(){
            self.location.href=$(this).data('href');
        });
    }
});
</script>
<?php
if($page=='podcasts-seasons'){
	echo '<script src="js/jquery.podcast-player.js?v='.get_cache_prevent_string().'"></script>';
	}
elseif($page=='radio'){
	echo '<script src="js/jquery.radio-player.js?v='.get_cache_prevent_string().'"></script>';
	}
?>
<script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
<script type="text/javascript" src="js/jQuery.loader.js"></script>
<script type="text/javascript" src="js/openpay.js"></script>

<?php if ($page == 'open-pay') : ?>
<script type="text/javascript">
	getCustomerId();
</script>
<?php endif; ?>	